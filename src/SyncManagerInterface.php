<?php

namespace Drupal\dataflow;

/**
 * The dataflow sync manager service interface.
 */
interface SyncManagerInterface {

  /**
   * Ensure given entity is exported.
   *
   * This method will *not* sync object immediately if it's already exported.
   * Instead, it will return a list of IDs.
   *
   * @param string $entityType
   *   Entity type.
   * @param string $destinationId
   *   Destination plugin ID.
   * @param string $exportType
   *   Export type.
   * @param int|array $entityId
   *   Entity ID or an array of IDs.
   * @param bool $forceUpdateChanged
   *   Set TRUE to make sure exported entity is up to date. By default, this
   *   method may skip re-export if the entity has changed since last sync.
   *
   * @return array
   *   An array of entity ID => Remote object ID.
   *
   * @throws \Drupal\dataflow\Plugin\Exception\MissingPluginException
   *   Missing sync plugin.
   * @throws \Drupal\dataflow\Exception\ExportException
   *   Export failure.
   */
  public function export($entityType, $destinationId, $exportType, $entityId, $forceUpdateChanged = FALSE);

  /**
   * Registers entities in the sync table with the NOT_SYNCED status.
   *
   * Registered entities won't be exported on cron.
   *
   * @param string $entityType
   *   Entity type.
   * @param string $destinationId
   *   Destination plugin ID.
   * @param string $exportType
   *   Export type.
   * @param int|array $entityIds
   *   Entity ID or an array of IDs.
   */
  public function registerEntity($entityType, $destinationId, $exportType, $entityIds);

  /**
   * Ensure given entity is synced.
   *
   * Unlike export(), this method will force re-export object
   * *immediately* if it's not in sync.
   *
   * @param string $entityType
   *   Entity type.
   * @param string $destinationId
   *   Destination plugin ID.
   * @param string $exportType
   *   Export type.
   * @param int|array $entityIds
   *   Entity ID or an array of IDs.
   * @param bool $recursiveExport
   *   Whether given entity is a dependency for something.
   *   Setting this to TRUE will enable throwing sync exclusion exceptions
   *   which may be then caught by the parent exporter process.
   *
   * @return array
   *   An array of entity ID => Remote object ID.
   *
   * @throws \Drupal\dataflow\Plugin\Exception\MissingPluginException
   *   Missing sync plugin.
   * @throws \Drupal\dataflow\Exception\SyncExcludedException
   *   Entity is excluded from sync.
   * @throws \Drupal\dataflow\Exception\ExportException
   *   Export failure.
   */
  public function sync($entityType, $destinationId, $exportType, $entityIds, $recursiveExport = FALSE);

  /**
   * Enqueue syncing the entity.
   *
   * The real sync may occur either at Drupal shutdown or Cron.
   *
   * @param string $entityType
   *   Entity type.
   * @param string $destinationId
   *   Destination plugin ID.
   * @param string $exportType
   *   Export type.
   * @param int|array $entityIds
   *   Entity ID or an array of IDs.
   */
  public function delayedSync($entityType, $destinationId, $exportType, $entityIds);

  /**
   * Remove the item from shutdown sync queue.
   *
   * This function is an opposite to delayedSync() and may be called by
   * export() to remove items from the queue.
   *
   * @param string $entityType
   *   Entity type.
   * @param string $destinationId
   *   Destination plugin ID.
   * @param string $exportType
   *   Export type.
   * @param int|array $entityIds
   *   Entity ID or an array of IDs.
   *
   * @see \Drupal\dataflow\SyncManagerInterface::delayedSync()
   */
  public function unsetDelayedSync($entityType, $destinationId, $exportType, $entityIds);

  /**
   * Run all sync actions enqueued by delayedSync() and flush the queue.
   *
   * @see \Drupal\dataflow\SyncManagerInterface::delayedSync()
   */
  public function syncAndFlush();

  /**
   * Flushes the shutdown sync queue.
   *
   * @see \Drupal\dataflow\SyncManagerInterface::delayedSync()
   */
  public function flush();

  /**
   * Run all pending sync actions on Cron.
   *
   * @see \Drupal\dataflow\SyncManagerInterface::delayedSync()
   */
  public function syncOnCron();

}
