<?php

namespace Drupal\dataflow;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Database\Connection;
use Drupal\dataflow\Event\SyncStatusSaveEvent;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Dataflow sync mapping manager service.
 */
class MappingManager implements MappingManagerInterface {

  /**
   * Database connection service.
   *
   * @var \Drupal\Core\Database\Driver\mysql\Connection
   */
  protected $database;

  /**
   * Event dispatcher.
   *
   * @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a new MappingManager object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   Database connection.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   Time service.
   * @param \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   */
  public function __construct(Connection $database, TimeInterface $time, EventDispatcherInterface $event_dispatcher) {
    $this->database = $database;
    $this->eventDispatcher = $event_dispatcher;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public function getSyncStatus($entityType, $destinationId, $exportType, $entity_id) {
    if (!is_array($entity_id)) {
      $entity_id = [$entity_id];
    }

    $status = $this
      ->database
      ->select('dataflow', 's')
      ->fields('s', [
        'entity_id',
        'status',
        'remote_id',
        'sync_time',
        'fail_time',
        'fields_hash',
      ])
      ->condition('entity_type', $entityType)
      ->condition('destination', $destinationId)
      ->condition('export_type', $exportType)
      ->condition('entity_id', $entity_id, 'IN')
      ->execute()
      ->fetchAllAssoc('entity_id', \PDO::FETCH_ASSOC);

    return $status + array_fill_keys($entity_id, FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function getIdMap($entityType, $destinationId, $exportType, $entity_id) {
    if (!is_array($entity_id)) {
      $entity_id = [$entity_id];
    }

    $status = $this
      ->database
      ->select('dataflow', 's')
      ->fields('s', [
        'entity_id',
        'remote_id',
      ])
      ->condition('entity_type', $entityType)
      ->condition('destination', $destinationId)
      ->condition('export_type', $exportType)
      ->condition('entity_id', $entity_id, 'IN')
      ->execute()
      ->fetchAllKeyed();

    return $status + array_fill_keys($entity_id, FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function setSyncStatus($entityType, $destinationId, $exportType, array $idMap, $status, $cronExport = TRUE, array $fieldsHash = []) {
    $this->assertStatusValue($status);

    foreach ($idMap as $entityId => $remoteId) {
      $fields = [
        'status' => $status,
        'cron_export' => $cronExport ? static::CRON_EXPORT_ENABLED : static::CRON_EXPORT_DISABLED,
      ];
      if (isset($fieldsHash[$entityId])) {
        $fields['fields_hash'] = $fieldsHash[$entityId];
      }

      // Unset fail time by default.
      $fields['fail_time'] = 0;
      switch ($status) {
        case static::STATUS_SYNCED:
          $fields['sync_time'] = $this->time->getRequestTime();
          break;

        case static::STATUS_FAILED:
          // Update fail timestamp to move failed items to the end of the queue.
          $fields['fail_time'] = $this->time->getRequestTime();
          break;
      }
      if ($remoteId !== NULL) {
        // Only set remote ID if it's not NULL.
        $fields['remote_id'] = $remoteId;
      }
      if ($status == static::STATUS_DELETED) {
        // Force unset remote ID if object was deleted from remote API.
        $fields['remote_id'] = NULL;
      }

      $this
        ->database
        ->merge('dataflow')
        ->keys([
          'entity_type' => $entityType,
          'destination' => $destinationId,
          'export_type' => $exportType,
          'entity_id' => $entityId,
        ])
        ->fields($fields)
        ->execute();
    }

    $this->eventDispatcher->dispatch(new SyncStatusSaveEvent($entityType, $destinationId, $exportType, $idMap, $status, $cronExport), SyncStatusSaveEvent::STATUS_SAVE);
  }

  /**
   * Asserts that sync status is correct.
   *
   * @param int $status
   *   New sync status. May be either STATUS_NOT_SYNCED or STATUS_SYNCED.
   */
  protected function assertStatusValue($status) {
    $valid_statuses = [
      static::STATUS_NOT_SYNCED,
      static::STATUS_IN_PROGRESS,
      static::STATUS_SYNCED,
      static::STATUS_FAILED,
      static::STATUS_SYNC_EXCLUDED,
      static::STATUS_DELETION_IN_PROGRESS,
      static::STATUS_DELETED,
      static::STATUS_ENTITY_LOAD_ERROR,
    ];

    if (!is_numeric($status) || !in_array($status, $valid_statuses)) {
      throw new \InvalidArgumentException('Incorrect sync status.');
    }
  }

  /**
   * Returns a query for fetching a sync queue.
   *
   * @param bool|null $cron_export
   *   Controls fetching items exported on cron.
   *   Possible values are:
   *
   *   - NULL: get all items (do not apply condition).
   *   - TRUE: get items which should be exported on cron.
   *   - FALSE: get all items excluded from cron export.
   *
   * @return \Drupal\Core\Database\Query\SelectInterface
   *   A database query object.
   */
  protected function getSyncQueueQuery($cron_export) {
    $query = $this
      ->database
      ->select('dataflow', 's')
      ->fields('s', [
        'entity_type',
        'destination',
        'export_type',
        'entity_id',
      ])
      // Move failed items to the end.
      ->orderBy('fail_time', 'ASC');

    if ($cron_export !== NULL) {
      $cron_export = $cron_export ? static::CRON_EXPORT_ENABLED : static::CRON_EXPORT_DISABLED;
      $query->condition('cron_export', $cron_export, '=');
    }

    $query->condition('status', [
      static::STATUS_SYNCED,
      static::STATUS_SYNC_EXCLUDED,
      static::STATUS_DELETED,
      static::STATUS_ENTITY_LOAD_ERROR,
    ], 'NOT IN');

    $fail_timestamp_condition = $query->orConditionGroup();
    $fail_timestamp_condition->condition('status', static::STATUS_FAILED, '<>')
      // @todo Move 1800 (30 minutes) to config.
      ->condition('fail_time', $this->time->getRequestTime() - 1800, '<=');

    // Exclude failures happened in last 30 minutes.
    $query->condition($fail_timestamp_condition);

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function countSyncQueue($cron_export) {
    $query = $this->getSyncQueueQuery($cron_export);
    return $query->countQuery()->execute()->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function getSyncQueue($limit, $cron_export) {
    $query = $this->getSyncQueueQuery($cron_export)->range(0, $limit);

    $queue = [];
    $result = $query->execute();
    while ($row = $result->fetchAssoc()) {
      $queue[$row['entity_type']][$row['destination']][$row['export_type']][$row['entity_id']] = $row['entity_id'];
    }

    return $queue;
  }

  /**
   * {@inheritdoc}
   */
  public function findMappedEntities($destinationId, array $remoteIds) {
    $return = array_fill_keys($remoteIds, FALSE);

    /** @var \Drupal\Core\Database\Query\Select $query */
    $query = $this
      ->database
      ->select('dataflow', 's')
      ->fields('s', [
        'remote_id',
        'entity_type',
        'export_type',
        'entity_id',
      ])
      ->condition('destination', $destinationId)
      ->condition('remote_id', $remoteIds, 'IN');

    $result = $query->execute();
    while ($row = $result->fetchObject()) {
      $return[$row->remote_id][$row->entity_type][$row->export_type] = $row->entity_id;
    }

    return $return;
  }

}
