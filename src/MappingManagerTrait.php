<?php

namespace Drupal\dataflow;

/**
 * Allows setter injection and simple usage of the service.
 */
trait MappingManagerTrait {

  /**
   * The sync objects mapping manager.
   *
   * @var \Drupal\dataflow\MappingManagerInterface
   */
  protected $mappingManager;

  /**
   * Sets the sync objects mapping manager.
   *
   * @param \Drupal\dataflow\MappingManagerInterface $mappingManager
   *   The sync objects mapping manager.
   *
   * @return $this
   */
  public function setMappingManager(MappingManagerInterface $mappingManager) {
    $this->mappingManager = $mappingManager;
    return $this;
  }

  /**
   * Gets the sync objects mapping manager.
   *
   * @return \Drupal\dataflow\MappingManagerInterface
   *   The sync objects mapping manager.
   */
  public function getMappingManager() {
    if (empty($this->mappingManager)) {
      $this->mappingManager = \Drupal::service('dataflow.mapping');
    }
    return $this->mappingManager;
  }

}
