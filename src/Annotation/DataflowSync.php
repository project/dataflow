<?php

namespace Drupal\dataflow\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Dataflow entity sync item annotation object.
 *
 * @see \Drupal\dataflow\Plugin\SyncPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class DataflowSync extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The entity type.
   *
   * @var string
   */
  public $entityType;

  /**
   * Destination plugin ID.
   *
   * @var string
   */
  public $destination;

  /**
   * Destination plugin settings.
   *
   * @var array
   */
  public $destinationSettings = [];

  /**
   * Export type. May be used to export same entity multiple times.
   *
   * @var string
   */
  public $exportType = 'default';

  /**
   * Whether to track fields changes.
   *
   * May be used to prevent re-exporting unchanged entities.
   *
   * @var bool
   */
  public $trackFields = FALSE;

}
