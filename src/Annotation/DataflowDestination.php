<?php

namespace Drupal\dataflow\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines Dataflow destination plugin annotation object.
 *
 * @see \Drupal\dataflow\Plugin\DestinationPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class DataflowDestination extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the destination plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label;

  /**
   * A short human readable description of the destination plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $description;

}
