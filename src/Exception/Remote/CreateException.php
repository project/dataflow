<?php

namespace Drupal\dataflow\Exception\Remote;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Component\Serialization\Json;
use Drupal\dataflow\Plugin\DestinationInterface;

/**
 * The remote entity creation exception.
 */
class CreateException extends RemoteException implements HasFieldsDataInterface {

  /**
   * The fields that were exported.
   *
   * @var array
   */
  protected array $fields;

  /**
   * Construct the new CreateException object.
   *
   * @param \Drupal\dataflow\Plugin\DestinationInterface $destination
   *   The destination plugin.
   * @param string $message
   *   The Exception message to throw.
   * @param array $fields
   *   The fields that were exported.
   * @param \Throwable|null $previous
   *   The previous throwable used for the exception chaining.
   */
  public function __construct(
    DestinationInterface $destination,
    $message,
    array $fields = [],
    \Throwable $previous = NULL
  ) {
    parent::__construct(
      $destination,
      $message,
      $previous
    );

    $this->fields = $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getFields() {
    return $this->fields;
  }

  /**
   * {@inheritdoc}
   */
  protected function getExceptionMessage() {
    $arguments = [
      '%message' => $this->getMessage(),
      '%destination' => $this->getDestinationId(),
      '%fields' => Json::encode($this->getFields()),
    ];
    return (string) (new FormattableMarkup('Remote entity creation error. Message: %message. Destination: %destination, exported fields: %fields.', $arguments));
  }

}
