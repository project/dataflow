<?php

namespace Drupal\dataflow\Exception\Remote;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\dataflow\Plugin\DestinationInterface;

/**
 * The remote entity deletion exception.
 */
class DeleteException extends RemoteException implements HasRemoteIdInterface {

  /**
   * The remote deleted object ID.
   *
   * @var string
   */
  protected $remoteId;

  /**
   * Construct the new DeleteException object.
   *
   * @param \Drupal\dataflow\Plugin\DestinationInterface $destination
   *   The destination plugin.
   * @param string $remoteId
   *   The remote object ID.
   * @param string $message
   *   The Exception message to throw.
   * @param \Throwable|null $previous
   *   The previous throwable used for the exception chaining.
   */
  public function __construct(
    DestinationInterface $destination,
    $remoteId,
    $message = '',
    \Throwable $previous = NULL
  ) {
    parent::__construct(
      $destination,
      $message,
      $previous
    );
    $this->remoteId = $remoteId;
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteId() {
    return $this->remoteId;
  }

  /**
   * {@inheritdoc}
   */
  protected function getExceptionMessage() {
    $arguments = [
      '%message' => $this->getMessage(),
      '%destination' => $this->getDestinationId(),
      '%remote_id' => $this->getRemoteId(),
    ];
    return (string) (new FormattableMarkup('Remote entity deletion error. Message: %message. Destination: %destination, remote ID: %remote_id.', $arguments));
  }

}
