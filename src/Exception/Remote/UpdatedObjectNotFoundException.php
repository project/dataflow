<?php

namespace Drupal\dataflow\Exception\Remote;

/**
 * Exception thrown when the remote updated object is not found.
 */
class UpdatedObjectNotFoundException extends UpdateException {}
