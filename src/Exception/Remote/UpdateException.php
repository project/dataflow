<?php

namespace Drupal\dataflow\Exception\Remote;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Component\Serialization\Json;
use Drupal\dataflow\Plugin\DestinationInterface;

/**
 * The remote entity update exception.
 */
class UpdateException extends RemoteException implements HasRemoteIdInterface, HasFieldsDataInterface {

  /**
   * The remote updated object ID.
   *
   * @var string
   */
  protected $remoteId;

  /**
   * The fields that were exported.
   *
   * @var array
   */
  protected array $fields;

  /**
   * Construct the new UpdateException object.
   *
   * @param \Drupal\dataflow\Plugin\DestinationInterface $destination
   *   The destination plugin.
   * @param string $message
   *   The Exception message to throw.
   * @param string $remoteId
   *   The remote object ID.
   * @param array $fields
   *   The fields that were exported.
   * @param \Throwable|null $previous
   *   The previous throwable used for the exception chaining.
   */
  public function __construct(
    DestinationInterface $destination,
    $message,
    $remoteId,
    array $fields = [],
    \Throwable $previous = NULL
  ) {
    parent::__construct(
      $destination,
      $message,
      $previous
    );
    $this->remoteId = $remoteId;
    $this->fields = $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteId() {
    return $this->remoteId;
  }

  /**
   * {@inheritdoc}
   */
  public function getFields() {
    return $this->fields;
  }

  /**
   * {@inheritdoc}
   */
  protected function getExceptionMessage() {
    $arguments = [
      '%message' => $this->getMessage(),
      '%destination' => $this->getDestinationId(),
      '%remote_id' => $this->getRemoteId(),
      '%fields' => Json::encode($this->getFields()),
    ];
    return (string) (new FormattableMarkup('Remote entity update error. Message: %message. Destination: %destination, remote ID: %remote_id, exported fields: %fields.', $arguments));
  }

}
