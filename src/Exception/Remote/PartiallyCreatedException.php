<?php

namespace Drupal\dataflow\Exception\Remote;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\dataflow\Plugin\DestinationInterface;

/**
 * Exception thrown when a remote destination is partially created.
 *
 * Used to resolve circular dependencies.
 */
class PartiallyCreatedException extends RemoteException implements HasRemoteIdInterface, HasSkippedDependenciesInterface {

  /**
   * The remote created object ID.
   *
   * @var string
   */
  protected $remoteId;

  /**
   * The list of dependencies that were skipped.
   *
   * @var array
   */
  protected $skippedDependencies;

  /**
   * Construct the new PartiallyCreatedException object.
   *
   * @param \Drupal\dataflow\Plugin\DestinationInterface $destination
   *   The destination plugin.
   * @param string $remoteId
   *   The remote object ID.
   * @param array $skippedDependencies
   *   The list of dependencies that were skipped.
   * @param string $message
   *   The Exception message to throw.
   * @param \Throwable|null $previous
   *   The previous throwable used for the exception chaining.
   */
  public function __construct(
    DestinationInterface $destination,
    $remoteId,
    array $skippedDependencies,
    $message = '',
    \Throwable $previous = NULL
  ) {
    parent::__construct(
      $destination,
      $message,
      $previous
    );

    $this->remoteId = $remoteId;
    $this->skippedDependencies = $skippedDependencies;
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteId() {
    return $this->remoteId;
  }

  /**
   * {@inheritdoc}
   */
  public function getSkippedDependencies() {
    return $this->skippedDependencies;
  }

  /**
   * {@inheritdoc}
   */
  protected function getExceptionMessage() {
    $arguments = [
      '%message' => $this->getMessage(),
      '%destination' => $this->getDestinationId(),
      '%remote_id' => $this->getRemoteId(),
    ];
    return (string) (new FormattableMarkup('Entity created par. Message: %message. Destination: %destination, remote ID: %remote_id.', $arguments));
  }

}
