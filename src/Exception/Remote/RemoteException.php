<?php

namespace Drupal\dataflow\Exception\Remote;

use Drupal\dataflow\Exception\DataflowExceptionBase;
use Drupal\dataflow\Exception\HasDestinationInterface;
use Drupal\dataflow\Plugin\DestinationInterface;

/**
 * Base exception class for remote objects actions.
 */
abstract class RemoteException extends DataflowExceptionBase implements HasDestinationInterface {

  /**
   * The destination plugin.
   *
   * @var \Drupal\dataflow\Plugin\DestinationInterface
   */
  protected $destination;

  /**
   * Construct the new RemoteException object.
   *
   * @param \Drupal\dataflow\Plugin\DestinationInterface $destination
   *   The destination plugin instance.
   * @param string $message
   *   The Exception message to throw.
   * @param \Throwable|null $previous
   *   The previous throwable used for the exception chaining.
   */
  public function __construct(
    DestinationInterface $destination,
    $message = '',
    \Throwable $previous = NULL
  ) {
    parent::__construct($message, 0, $previous);
    $this->destination = $destination;
  }

  /**
   * {@inheritdoc}
   */
  public function getDestinationId() {
    return $this->destination->getPluginId();
  }

  /**
   * {@inheritdoc}
   */
  public function getDestination() {
    return $this->destination;
  }

}
