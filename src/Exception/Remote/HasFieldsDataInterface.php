<?php

namespace Drupal\dataflow\Exception\Remote;

/**
 * Common interface for remote exceptions that have fields data.
 */
interface HasFieldsDataInterface {

  /**
   * Returns the remote fields.
   *
   * @return array
   *   The remote fields.
   */
  public function getFields();

}
