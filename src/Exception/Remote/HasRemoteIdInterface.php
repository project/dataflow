<?php

namespace Drupal\dataflow\Exception\Remote;

/**
 * Common interface for remote exceptions that has known remote object ID.
 */
interface HasRemoteIdInterface {

  /**
   * Gets the remote object ID.
   *
   * @return int
   *   The remote ID.
   */
  public function getRemoteId();

}
