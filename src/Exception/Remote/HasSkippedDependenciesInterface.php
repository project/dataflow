<?php

namespace Drupal\dataflow\Exception\Remote;

/**
 * Common interface for remote exceptions that has skipped dependencies.
 */
interface HasSkippedDependenciesInterface {

  /**
   * Returns the list of dependencies that were skipped.
   *
   * @return array
   *   The list of dependencies that were skipped.
   */
  public function getSkippedDependencies();

}
