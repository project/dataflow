<?php

namespace Drupal\dataflow\Exception;

use Drupal\Component\Render\FormattableMarkup;

/**
 * Sync exclusion exception.
 *
 * This exception may be thrown if given entity shouldn't be synced for some
 * reason.
 */
class SyncExcludedException extends ExportProcessException {

  /**
   * {@inheritdoc}
   */
  protected function getExceptionMessage() {
    $arguments = [
      '%entity_type' => $this->getEntityType(),
      '%destination' => $this->getDestinationId(),
      '%export_type' => $this->getExportType(),
      '%id' => $this->getEntityId(),
    ];
    return (string) (new FormattableMarkup('Entity is excluded from sync. Entity type: %entity_type, destination: %destination, export type: %export_type, entity ID: %id.', $arguments));
  }

}
