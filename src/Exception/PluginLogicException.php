<?php

namespace Drupal\dataflow\Exception;

use Drupal\Component\Render\FormattableMarkup;

/**
 * Plugin logic exception.
 *
 * This exception may be thrown if inconsistency in plugin logic is detected,
 * typically indicating a bug.
 */
class PluginLogicException extends ExportProcessException {

  /**
   * {@inheritdoc}
   */
  protected function getExceptionMessage() {
    $arguments = [
      '%message' => $this->getMessage(),
      '%entity_type' => $this->getEntityType(),
      '%destination' => $this->getDestinationId(),
      '%id' => $this->getEntityId(),
    ];
    return (string) (new FormattableMarkup('Sync plugin logic error. Message: %message. Entity type: %entity_type, destination: %destination, entity ID: %id.', $arguments));
  }

}
