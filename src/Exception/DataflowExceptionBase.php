<?php

namespace Drupal\dataflow\Exception;

/**
 * Base class for all Dataflow exceptions.
 */
abstract class DataflowExceptionBase extends \Exception implements HasFormattedLogMessageInterface {

  /**
   * Get error message for logging in Drupal.
   *
   * @return string
   *   Error message.
   */
  public function getLogMessage() {
    $message = get_class($this) . ' thrown:' . PHP_EOL;
    $message .= $this->getExceptionMessage();

    if ($previous = $this->getPrevious()) {
      $message .= PHP_EOL . 'Previous exception:' . PHP_EOL;
      if ($previous instanceof HasFormattedLogMessageInterface) {
        $message .= $previous->getLogMessage();
      }
      else {
        $message .= $previous->getMessage();
      }
    }

    return $message;
  }

  /**
   * Get human-readable error message.
   *
   * @return string
   *   Error message.
   */
  abstract protected function getExceptionMessage();

}
