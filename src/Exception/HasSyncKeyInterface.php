<?php

namespace Drupal\dataflow\Exception;

/**
 * The common interface for exceptions having a sync key.
 */
interface HasSyncKeyInterface {

  /**
   * Get key of the sync process which have caused this exception.
   *
   * @return string
   *   Sync process key.
   */
  public function getSyncKey();

}
