<?php

namespace Drupal\dataflow\Exception;

/**
 * Interface for exceptions that have a formatted log message.
 */
interface HasFormattedLogMessageInterface {

  /**
   * Get error message for logging in Drupal.
   *
   * @return string
   *   Error message.
   */
  public function getLogMessage();

}
