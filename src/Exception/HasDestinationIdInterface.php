<?php

namespace Drupal\dataflow\Exception;

/**
 * Common interface for Dataflow exceptions having destination plugin ID.
 */
interface HasDestinationIdInterface {

  /**
   * Returns the destination plugin ID.
   *
   * @return string
   *   The destination plugin ID.
   */
  public function getDestinationId();

}
