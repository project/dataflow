<?php

namespace Drupal\dataflow\Exception;

/**
 * Common interface for Dataflow exceptions having destination plugin instance.
 */
interface HasDestinationInterface extends HasDestinationIdInterface {

  /**
   * Returns the destination.
   *
   * @return \Drupal\dataflow\Plugin\DestinationInterface
   *   The destination.
   */
  public function getDestination();

}
