<?php

namespace Drupal\dataflow\Exception;

use Drupal\dataflow\Plugin\DestinationInterface;

/**
 * Abstract base class for exceptions thrown by export processes.
 *
 * Unlike ExportException, this exception is thrown when the destination plugin
 * is already available.
 */
abstract class ExportProcessException extends ExportException implements HasDestinationInterface {

  /**
   * The destination plugin.
   *
   * @var \Drupal\dataflow\Plugin\DestinationInterface
   */
  protected $destination;

  /**
   * Construct the new ExportProcessException object.
   *
   * @param string $entityType
   *   The Drupal entity type.
   * @param \Drupal\dataflow\Plugin\DestinationInterface $destination
   *   The destination plugin.
   * @param string $exportType
   *   The export type.
   * @param int $entityId
   *   The entity ID.
   * @param string $message
   *   The Exception message to throw.
   * @param \Throwable|null $previous
   *   The previous throwable used for the exception chaining.
   */
  public function __construct(
    $entityType,
    DestinationInterface $destination,
    $exportType,
    $entityId,
    $message = '',
    \Throwable $previous = NULL
  ) {
    parent::__construct(
      $entityType,
      $destination->getPluginId(),
      $exportType,
      $entityId,
      $message,
      $previous
    );
  }

  /**
   * Get destination plugin.
   */
  public function getDestination() {
    return $this->destination;
  }

}
