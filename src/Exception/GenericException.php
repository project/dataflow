<?php

namespace Drupal\dataflow\Exception;

use Drupal\Component\Render\FormattableMarkup;

/**
 * Generic exception class.
 *
 * May be used to log custom errors.
 */
class GenericException extends ExportProcessException {

  /**
   * {@inheritdoc}
   */
  protected function getExceptionMessage() {
    $arguments = [
      '%message' => $this->getMessage(),
      '%entity_type' => $this->getEntityType(),
      '%destination' => $this->getDestinationId(),
      '%export_type' => $this->getExportType(),
      '%id' => $this->getEntityId(),
    ];
    return (string) (new FormattableMarkup('Generic export error. Message: %message. Entity type: %entity_type, destination: %destination, export type: %export_type, entity ID: %id.', $arguments));
  }

}
