<?php

namespace Drupal\dataflow\Exception;

use Drupal\Component\Render\FormattableMarkup;

/**
 * Removal request exception.
 *
 * This exception may be thrown if the sync plugin requests remote object
 * removal.
 */
class RemovalRequestException extends ExportProcessException {

  /**
   * {@inheritdoc}
   */
  protected function getExceptionMessage() {
    $arguments = [
      '%entity_type' => $this->getEntityType(),
      '%destination' => $this->getDestinationId(),
      '%export_type' => $this->getExportType(),
      '%id' => $this->getEntityId(),
    ];
    return (string) (new FormattableMarkup('Remote object is pending removal. Entity type: %entity_type, destination: %destination, export type: %export_type, entity ID: %id.', $arguments));
  }

}
