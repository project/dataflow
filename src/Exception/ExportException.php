<?php

namespace Drupal\dataflow\Exception;

use Drupal\dataflow\Util\SyncKey;

/**
 * Generic export exception class.
 */
abstract class ExportException extends DataflowExceptionBase implements HasDestinationIdInterface, HasSyncKeyInterface {

  /**
   * The exported entity type.
   *
   * @var string
   */
  protected $entityType;

  /**
   * The exported entity ID.
   *
   * @var int
   */
  protected $entityId;

  /**
   * The destination plugin ID.
   *
   * @var string
   */
  protected $destinationId;

  /**
   * The export type.
   *
   * @var string
   */
  protected $exportType;

  /**
   * Construct the new ExportException object.
   *
   * @param string $entityType
   *   The Drupal entity type.
   * @param string $destinationId
   *   The destination plugin ID.
   * @param string $exportType
   *   The export type.
   * @param int $entityId
   *   The entity ID.
   * @param string $message
   *   The Exception message to throw.
   * @param \Throwable|null $previous
   *   The previous throwable used for the exception chaining.
   */
  public function __construct(
    $entityType,
    $destinationId,
    $exportType,
    $entityId,
    $message = '',
    \Throwable $previous = NULL
  ) {
    parent::__construct($message, 0, $previous);
    $this->entityType = $entityType;
    $this->destinationId = $destinationId;
    $this->exportType = $exportType;
    $this->entityId = $entityId;
  }

  /**
   * Get entity type.
   */
  public function getEntityType() {
    return $this->entityType;
  }

  /**
   * Get destination plugin ID.
   */
  public function getDestinationId() {
    return $this->destinationId;
  }

  /**
   * Get export type.
   */
  public function getExportType() {
    return $this->exportType;
  }

  /**
   * Get entity ID.
   */
  public function getEntityId() {
    return $this->entityId;
  }

  /**
   * Get key of the sync process which have caused this exception.
   *
   * @return string
   *   Sync process key.
   */
  public function getSyncKey() {
    return SyncKey::getSyncKey(
      $this->getEntityType(),
      $this->getDestinationId(),
      $this->getExportType(),
      $this->getEntityId(),
    );
  }

}
