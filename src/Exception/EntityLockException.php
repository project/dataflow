<?php

namespace Drupal\dataflow\Exception;

use Drupal\Component\Render\FormattableMarkup;

/**
 * Sync lock exception.
 */
class EntityLockException extends ExportException {

  /**
   * {@inheritdoc}
   */
  protected function getExceptionMessage() {
    $arguments = [
      '%entity_type' => $this->getEntityType(),
      '%destination' => $this->getDestinationId(),
      '%export_type' => $this->getExportType(),
      '%id' => $this->getEntityId(),
    ];
    return (string) (new FormattableMarkup('Failed acquiring item lock. Entity type: %entity_type, destination: %destination, export type: %export_type, entity ID: %id.', $arguments));
  }

}
