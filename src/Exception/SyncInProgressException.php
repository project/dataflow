<?php

namespace Drupal\dataflow\Exception;

use Drupal\Component\Render\FormattableMarkup;

/**
 * Sync in progress exception.
 *
 * This exception may be thrown if SyncBase::getReferencedEntityRemoteId() is
 * called for an entity which is being exported right now and thus doesn't have
 * an ID.
 *
 * This exception is caught by SyncBase::export().
 *
 * @see \Drupal\dataflow\Plugin\SyncBase::getReferencedEntityRemoteId()
 * @see \Drupal\dataflow\Plugin\SyncBase::export()
 */
class SyncInProgressException extends ExportException {

  /**
   * {@inheritdoc}
   */
  protected function getExceptionMessage() {
    $arguments = [
      '%entity_type' => $this->getEntityType(),
      '%destination' => $this->getDestinationId(),
      '%export_type' => $this->getExportType(),
      '%id' => $this->getEntityId(),
    ];
    return (string) (new FormattableMarkup('Entity sync is already in progress. Entity type: %entity_type, destination: %destination, export type: %export_type, entity ID: %id.', $arguments));
  }

}
