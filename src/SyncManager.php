<?php

namespace Drupal\dataflow;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Utility\Timer;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\dataflow\Event\EntitySyncError;
use Drupal\dataflow\Exception\EntityLockException;
use Drupal\dataflow\Exception\ExportException;
use Drupal\dataflow\Exception\GenericException;
use Drupal\dataflow\Exception\RecursiveExportException;
use Drupal\dataflow\Exception\Remote\PartiallyCreatedException;
use Drupal\dataflow\Exception\Remote\RemoteException;
use Drupal\dataflow\Exception\RemovalRequestException;
use Drupal\dataflow\Exception\SyncExcludedException;
use Drupal\dataflow\Exception\SyncInProgressException;
use Drupal\dataflow\Plugin\SyncPluginManagerInterface;
use Drupal\dataflow\Util\SyncKey;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * The dataflow sync manager service.
 */
class SyncManager implements SyncManagerInterface {

  const CRON_TIMER = 'dataflow_cron';

  /**
   * Mapping manager.
   *
   * @var \Drupal\dataflow\MappingManagerInterface
   */
  protected $idMap;

  /**
   * Sync plugin manager.
   *
   * @var \Drupal\dataflow\Plugin\SyncPluginManagerInterface
   */
  protected $pluginManager;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Lock service.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected $lock;

  /**
   * The log channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $log;

  /**
   * Event dispatcher.
   *
   * @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * IDs of entities which are sync pending.
   *
   * @var array
   */
  protected $delayedSyncIds = [];

  /**
   * Flag indicating whether the shutdown function is registered.
   *
   * @var bool
   */
  protected $shutdownFunctionRegistered;

  /**
   * Constructs a new SyncManager object.
   */
  public function __construct(MappingManagerInterface $map, SyncPluginManagerInterface $plugin_manager, EntityTypeManagerInterface $entityType_manager, LockBackendInterface $lock, LoggerChannelFactoryInterface $logger_factory, EventDispatcherInterface $event_dispatcher) {
    $this->idMap = $map;
    $this->pluginManager = $plugin_manager;
    $this->entityTypeManager = $entityType_manager;
    $this->lock = $lock;
    $this->log = $logger_factory->get('dataflow');
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public function export($entityType, $destinationId, $exportType, $entityId, $forceUpdateChanged = FALSE) {
    if (!is_array($entityId)) {
      $entityId = [$entityId];
    }

    $this->exportMissing($entityType, $destinationId, $exportType, $entityId, $forceUpdateChanged);
    return $this->idMap->getIdMap($entityType, $destinationId, $exportType, $entityId);
  }

  /**
   * {@inheritdoc}
   */
  public function registerEntity($entityType, $destinationId, $exportType, $entityIds) {
    // Make sure there's a plugin, fail early otherwise.
    $this->pluginManager->getInstanceByType($entityType, $destinationId, $exportType);
    $entityIds = is_array($entityIds) ? $entityIds : [$entityIds];

    // Update database, set sync status to NOT_SYNCED.
    $this
      ->idMap
      ->setSyncStatus($entityType, $destinationId, $exportType, array_fill_keys($entityIds, NULL), MappingManagerInterface::STATUS_NOT_SYNCED, FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function sync($entityType, $destinationId, $exportType, $entityIds, $recursiveExport = FALSE) {
    if (!is_array($entityIds)) {
      $entityIds = [$entityIds];
    }
    elseif (empty($entityIds)) {
      // Empty array, nothing to do.
      return [];
    }

    $found = [];

    $syncPlugin = $this->pluginManager->getInstanceByType($entityType, $destinationId, $exportType);
    $entities = $this->entityTypeManager->getStorage($entityType)->loadMultiple($entityIds);
    foreach ($entities as $entity) {
      $entity_id = $entity->id();
      $found[] = $entity_id;
      $lock_id = $this->getLockId($entityType, $destinationId, $exportType, $entity_id);

      if (!$this->lock->acquire($lock_id)) {
        // Could not acquire the lock.
        throw new EntityLockException($entityType, $destinationId, $exportType, $entity_id);
      }

      // Set sync status to 'in progress'.
      $this->idMap->setSyncStatus($entityType, $destinationId, $exportType, [$entity_id => NULL], MappingManagerInterface::STATUS_IN_PROGRESS);

      try {
        $syncPlugin
          ->assertShouldSync($entity);
      }
      catch (SyncExcludedException $e) {
        // This entity shouldn't be synced. It's okay if the export was called
        // directly, just skip.
        $this->idMap->setSyncStatus(
          $entityType,
          $destinationId,
          $exportType,
          [$entity_id => NULL],
          MappingManagerInterface::STATUS_SYNC_EXCLUDED,
          TRUE,
          // Clear fields hash to force re-exporting if the next export would
          // happen someday in the future.
          [$entity_id => '']
        );
        $this->lock->release($lock_id);
        if ($recursiveExport) {
          // Re-throw the same exception so that it could be caught by parent
          // entity sync process.
          throw $e;
        }
        continue;
      }
      catch (RemovalRequestException $e) {
        // The object is pending removal.
        // Some sync plugins may request if the object is a virtual entry.
        // Referring such item is not allowed.
        $this->idMap->setSyncStatus($entityType, $destinationId, $exportType, [$entity_id => NULL], MappingManagerInterface::STATUS_DELETION_IN_PROGRESS);
        // @todo Handle delete error.
        $syncPlugin
          ->assertEntity($entity)
          ->deleteFromRemote($entity);
        $this->idMap->setSyncStatus($entityType, $destinationId, $exportType, [$entity_id => NULL], MappingManagerInterface::STATUS_DELETED);
        $this->lock->release($lock_id);
        if ($recursiveExport) {
          // Re-throw the same exception so that it could be caught by parent
          // entity sync process.
          throw $e;
        }
        continue;
      }

      try {
        // Export.
        // NOTE: This try...catch block is NOT merged with one above since
        // both assertShouldSync() and export() may throw SyncExcludedException.
        $exportResult = $syncPlugin
          ->assertEntity($entity)
          ->export($entity);
      }
      catch (PartiallyCreatedException $e) {
        // The entity was partially created; save its ID to the map (since we
        // now have it, even if it's incomplete) and re-export it again to add
        // missing dependencies (which should have been resolved at this
        // moment).
        $this
          ->idMap
          ->setSyncStatus(
            $entityType,
            $destinationId,
            $exportType,
            [$entity_id => $e->getRemoteId()],
            MappingManagerInterface::STATUS_IN_PROGRESS,
            TRUE,
            // Clear fields hash to force re-exporting if the next export would
            // happen someday in the future.
            [$entity_id => '']
          );

        // Re-export the entity again. This time, we should be able to create
        // it as all dependencies should be able to reference it now.
        $exportResult = $syncPlugin
          ->assertEntity($entity)
          ->export($entity);
      }
      catch (SyncInProgressException $e) {
        // Release the lock and explicitly state that this entity wasn't
        // exported. It would be handled later by the parent entity sync (which
        // would throw PartiallyCreatedException and re-export itself).
        $this->lock->release($lock_id);
        $this->idMap->setSyncStatus(
          $entityType,
          $destinationId,
          $exportType,
          [$entity_id => NULL],
          MappingManagerInterface::STATUS_NOT_SYNCED,
          TRUE,
          // Clear fields hash to force re-exporting if the next export would
          // happen someday in the future.
          [$entity_id => '']
        );

        $syncKey = implode(':', [
          $entityType,
          $destinationId,
          $exportType,
          $entity->id(),
        ]);
        throw $syncKey === $e->getSyncKey()
          ? $e
          : new SyncInProgressException($entityType, $destinationId, $exportType, $entity->id(), '', $e);
      }
      catch (\Exception $e) {
        // Catch any exceptions thrown by dependant plugins and wrap them into
        // own exception to provide entity information.
        // Set sync status to 'failed' and release the lock.
        $this->idMap->setSyncStatus(
          $entityType,
          $destinationId,
          $exportType,
          [$entity_id => NULL],
          MappingManagerInterface::STATUS_FAILED,
          TRUE,
          // Clear fields hash to force re-exporting if the next export would
          // happen someday in the future.
          [$entity_id => '']
        );
        $this->lock->release($lock_id);

        // Throw a new exception in case of recursive export; the parent
        // export should catch this exception fail as well.
        // Also throw a new exception if the caught one is not an expected one.
        if ($recursiveExport || !($e instanceof RemoteException)) {
          throw new RecursiveExportException($entityType, $destinationId, $exportType, $entity_id, $e);
        }
        // Otherwise, just log and continue with the next entity.
        else {
          $this->log->error('Export exception, message: ' . (new GenericException($entityType, $syncPlugin->getDestination(), $exportType, $entity_id, $e))->getLogMessage());
        }
        continue;
      }

      // Set sync status to 'synced'.
      $this->idMap->setSyncStatus(
        $entityType,
        $destinationId,
        $exportType,
        [$entity_id => $exportResult->getRemoteId()],
        MappingManagerInterface::STATUS_SYNCED,
        TRUE,
        [$entity_id => $exportResult->getFieldsHash()],
      );

      // Release the lock.
      $this->lock->release($lock_id);
    }

    if ($missing = array_diff($entityIds, $found)) {
      $this->idMap->setSyncStatus(
        $entityType,
        $destinationId,
        $exportType,
        array_fill_keys($missing, NULL),
        MappingManagerInterface::STATUS_ENTITY_LOAD_ERROR,
        TRUE,
        array_fill_keys($missing, '')
      );
    }

    return $this->idMap->getIdMap($entityType, $destinationId, $exportType, $entityIds);
  }

  /**
   * {@inheritdoc}
   */
  public function delayedSync($entityType, $destinationId, $exportType, $entityIds) {
    // Make sure there's a plugin, fail early otherwise.
    $this->pluginManager->getInstanceByType($entityType, $destinationId, $exportType);

    if (!is_array($entityIds)) {
      $entityIds = [$entityIds];
    }
    foreach ($entityIds as $id) {
      $this->delayedSyncIds[$entityType][$destinationId][$exportType][$id] = $id;
    }

    // Update database, set sync status to NOT_SYNCED.
    $this
      ->idMap
      ->setSyncStatus($entityType, $destinationId, $exportType, array_fill_keys($entityIds, NULL), MappingManagerInterface::STATUS_NOT_SYNCED);

    // Register shutdown function.
    if (!$this->shutdownFunctionRegistered) {
      drupal_register_shutdown_function([$this, 'syncAndFlush']);
      $this->shutdownFunctionRegistered = TRUE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function unsetDelayedSync($entityType, $destinationId, $exportType, $entityIds) {
    if (!is_array($entityIds)) {
      $entityIds = [$entityIds];
    }
    foreach ($entityIds as $id) {
      if (isset($this->delayedSyncIds[$entityType][$destinationId][$exportType][$id])) {
        $this->delayedSyncIds[$entityType][$destinationId][$exportType][$id] = FALSE;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function syncAndFlush() {
    $this->syncPending($this->delayedSyncIds);
    $this->delayedSyncIds = [];
  }

  /**
   * {@inheritdoc}
   */
  public function flush() {
    $this->delayedSyncIds = [];
  }

  /**
   * {@inheritdoc}
   */
  public function syncOnCron() {
    Timer::start(static::CRON_TIMER);
    // @todo Configurable Cron sync limit.
    // 10000 ms == 10s
    while (Timer::read(static::CRON_TIMER) <= 20000
      && $queue = $this->idMap->getSyncQueue(100, TRUE)) {
      $this->syncPending($queue);
    }
    Timer::stop(static::CRON_TIMER);
  }

  /**
   * Find and force export missing entities.
   *
   * @param string $entityType
   *   Entity type.
   * @param string $destinationId
   *   Destination plugin ID.
   * @param string $exportType
   *   Export type.
   * @param array $entityIds
   *   Array of IDs of entities.
   * @param bool $forceUpdateChanged
   *   Set TRUE to make sure exported entity is up-to-date. By default, this
   *   method may skip re-export if the entity has changed since last sync.
   *
   * @throws \Drupal\dataflow\Exception\ExportException
   *   Export exception.
   */
  protected function exportMissing($entityType, $destinationId, $exportType, array $entityIds, $forceUpdateChanged = FALSE) {
    $missing_ids = array_filter($this->idMap->getSyncStatus($entityType, $destinationId, $exportType, $entityIds),
      function ($row) use ($forceUpdateChanged) {
        // FALSE means entity was never exported.
        // 0 means entity export was executed but never completed.
        // If the entity is deleted at remote destination - reexport it again.
        return $row === FALSE
        || $row['remote_id'] === 0
        || $row['remote_id'] === NULL
        || $row['status'] == MappingManagerInterface::STATUS_FAILED
        || $row['status'] == MappingManagerInterface::STATUS_DELETED
        || ($row['status'] == MappingManagerInterface::STATUS_NOT_SYNCED && $forceUpdateChanged);
      });
    $missing_ids = array_keys($missing_ids);
    if ($missing_ids) {
      $this->sync($entityType, $destinationId, $exportType, $missing_ids, TRUE);

      // Avoid syncing same entity twice.
      $this->unsetDelayedSync($entityType, $destinationId, $exportType, $missing_ids);
    }
  }

  /**
   * Get sync lock ID.
   *
   * @param string $entityType
   *   Entity type.
   * @param string $destinationId
   *   Destination plugin ID.
   * @param string $exportType
   *   Export type.
   * @param int $entityId
   *   Entity ID.
   *
   * @return string
   *   Lock ID.
   */
  protected function getLockId($entityType, $destinationId, $exportType, $entityId) {
    return SyncKey::getSyncKey($entityType, $destinationId, $exportType, $entityId);
  }

  /**
   * Sync entities queued.
   *
   * @param array $queue
   *   Multi-dimensional array of items to sync.
   *   Entity type => Destination ID => Export type => Entity ID =>
   *   Entity ID/status.
   */
  protected function syncPending(array &$queue) {
    foreach ($queue as $entityType => $destinations) {
      foreach ($destinations as $destinationId => $exportTypes) {
        foreach ($exportTypes as $exportType => $entityIds) {
          // Exclude entities that were already synced.
          $entitiesSyncStatus = $this->idMap->getSyncStatus($entityType, $destinationId, $exportType, $entityIds);
          $entityIds = array_filter(array_map(function ($entityId) use ($entitiesSyncStatus) {
            return $entitiesSyncStatus[$entityId]['status'] != MappingManagerInterface::STATUS_SYNCED
              ? $entityId
              : NULL;
          }, $entityIds));
          try {
            $this
              ->entityTypeManager
              ->getStorage($entityType)
              ->loadMultiple($entityIds);
          }
          catch (InvalidPluginDefinitionException $e) {
            $this->log->error('Export exception while loading entities, message: ' . $e->getMessage());
            continue;
          }
          foreach ($entityIds as $entityId => $status) {
            if ($status === FALSE) {
              // Avoid syncing same entities twice. First, the entity may be
              // exported as a dependency, and second time it could be processed
              // individually.
              continue;
            }

            try {
              $this->sync($entityType, $destinationId, $exportType, $entityId);
            }
            catch (ExportException $e) {
              $this->log->error('Export exception, message: ' . $e->getLogMessage());
              $this->eventDispatcher->dispatch(new EntitySyncError($e), EntitySyncError::SYNC_ERROR);
            }
          }
        }
      }
    }
  }

}
