<?php

namespace Drupal\dataflow\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Wraps a dataflow status save event for event listeners.
 */
class SyncStatusSaveEvent extends Event {

  /**
   * Destination plugin ID.
   *
   * @var string
   */
  protected $destinationId;

  /**
   * Export type.
   *
   * @var string
   */
  protected $exportType;

  /**
   * Name of the event fired when setting a status an id map.
   *
   * This event allows modules to perform an action whenever the id map got
   * a sync status. The event listener method receives
   * a \Drupal\dataflow\Event\SyncStatusSaveEvent instance.
   *
   * @Event
   *
   * @see \Drupal\migrate\Event\MigrateMapSaveEvent
   *
   * @var string
   */
  const STATUS_SAVE = 'dataflow.status_save';

  /**
   * The entity type.
   *
   * @var string
   */
  protected $entityType;

  /**
   * An array of entity ID => remote object ID.
   *
   * @var array
   */
  protected $idMap;

  /**
   * The sync status.
   *
   * @var int
   * @see \Drupal\dataflow\MappingManagerInterface
   */
  protected $status;

  /**
   * A flag indicating whether the record should be exported on cron.
   *
   * @var bool
   * @see \Drupal\dataflow\MappingManagerInterface
   */
  protected $cronExport;

  /**
   * SyncStatusSaveEvent constructor.
   *
   * @param string $entityType
   *   The entity type.
   * @param string $destinationId
   *   Destination plugin ID.
   * @param string $exportType
   *   Export type.
   * @param array $id_map
   *   The id map.
   * @param int $status
   *   The sync status.
   * @param bool $cron_export
   *   A flag indicating whether the record should be exported on cron.
   */
  public function __construct($entityType, $destinationId, $exportType, array $id_map, $status, $cron_export) {
    $this->destinationId = $destinationId;
    $this->exportType = $exportType;
    $this->entityType = $entityType;
    $this->idMap = $id_map;
    $this->status = $status;
    $this->cronExport = $cron_export;
  }

  /**
   * Entity type getter.
   *
   * @return string
   *   The entity type.
   */
  public function getEntityType() {
    return $this->entityType;
  }

  /**
   * Id map getter.
   *
   * @return array
   *   The id map.
   */
  public function getIdMap() {
    return $this->idMap;
  }

  /**
   * Sync status getter.
   *
   * @return int
   *   The sync status.
   */
  public function getStatus() {
    return $this->status;
  }

  /**
   * Cron export getter.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  public function getCronExport() {
    return $this->cronExport;
  }

  /**
   * Get destination plugin ID.
   *
   * @return string
   *   Destination plugin ID.
   */
  public function getDestinationId() {
    return $this->destinationId;
  }

  /**
   * Export type getter.
   *
   * @return string
   *   Export type getter.
   */
  public function getExportType() {
    return $this->exportType;
  }

}
