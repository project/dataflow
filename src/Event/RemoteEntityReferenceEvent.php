<?php

namespace Drupal\dataflow\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Dataflow export event.
 */
class RemoteEntityReferenceEvent extends Event {

  const REMOTE_ENTITY_REFERENCE = 'dataflow.remote_entity_reference';

  /**
   * Entity type.
   *
   * @var string
   */
  protected $entityType;

  /**
   * Destination plugin ID.
   *
   * @var string
   */
  protected $destinationId;

  /**
   * Export type.
   *
   * @var string
   */
  protected $exportType;

  /**
   * Referencing entity ID.
   *
   * @var int
   */
  protected $entityId;

  /**
   * Remote entity ID.
   *
   * @var int
   */
  protected $remoteEntityId;

  /**
   * The list of dependencies that were skipped.
   *
   * @var array
   */
  protected $skippedDependencies;

  /**
   * Whether this dependency is required.
   *
   * @var bool
   */
  protected $required;

  /**
   * Event constructor.
   *
   * @param string $entityType
   *   Entity type.
   * @param string $destinationId
   *   Destination plugin ID.
   * @param string $exportType
   *   Export type.
   * @param int $entityId
   *   Referencing entity ID.
   * @param array $skippedDependencies
   *   The list of dependencies that were skipped.
   * @param bool $required
   *   Whether this dependency is required.
   */
  public function __construct($entityType, $destinationId, $exportType, $entityId, array $skippedDependencies, $required) {
    $this->entityType = $entityType;
    $this->destinationId = $destinationId;
    $this->exportType = $exportType;
    $this->entityId = $entityId;
    $this->skippedDependencies = $skippedDependencies;
    $this->required = $required;
  }

  /**
   * Export type getter.
   *
   * @return string
   *   Export type getter.
   */
  public function getExportType() {
    return $this->exportType;
  }

  /**
   * Entity type getter.
   *
   * @return string
   *   Entity type getter.
   */
  public function getEntityType() {
    return $this->entityType;
  }

  /**
   * Destination id getter.
   *
   * @return string
   *   Destination id getter.
   */
  public function getDestinationId() {
    return $this->destinationId;
  }

  /**
   * Entity ID getter.
   *
   * @return int
   *   Entity ID.
   */
  public function getEntityId() {
    return $this->entityId;
  }

  /**
   * Skipped dependencies getter.
   *
   * @return array
   *   Skipped dependencies getter.
   */
  public function getSkippedDependencies() {
    return $this->skippedDependencies;
  }

  /**
   * Check if required.
   *
   * @return bool
   *   TRUE if required.
   */
  public function isRequired() {
    return $this->required;
  }

  /**
   * Remote entity ID getter.
   *
   * @return int
   *   Remote entity ID.
   */
  public function getRemoteEntityId() {
    return $this->remoteEntityId ?: NULL;
  }

  /**
   * Remote entity ID setter.
   *
   * @param int $remoteEntityId
   *   Remote entity ID.
   */
  public function setRemoteEntityId($remoteEntityId) {
    $this->remoteEntityId = $remoteEntityId;
  }

  /**
   * Skipped dependencies setter.
   *
   * @param string $key
   *   Sync key.
   */
  public function setSkippedDependencies($key) {
    $this->skippedDependencies[$key] = $key;
  }

}
