<?php

namespace Drupal\dataflow\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Entity\EntityInterface;
use Drupal\dataflow\Plugin\DestinationInterface;

/**
 * Dataflow export event.
 */
class ExportEvent extends Event {

  const CREATE = 'dataflow.create';
  const UPDATE = 'dataflow.update';

  // Remote object was removed *due to a plugin request*.
  // This event type shouldn't be dispatched for removal triggered by Drupal
  // entity delete.
  const DELETE_REQUEST = 'dataflow.delete_request';

  /**
   * Destination plugin ID.
   *
   * @var \Drupal\dataflow\Plugin\DestinationInterface
   */
  protected $destination;

  /**
   * Export type.
   *
   * @var string
   */
  protected $exportType;

  /**
   * Exported entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * Remote object ID.
   *
   * @var string
   */
  protected $remoteObjectId;

  /**
   * Event constructor.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Exported entity object.
   * @param \Drupal\dataflow\Plugin\DestinationInterface $destination
   *   Destination plugin.
   * @param string $exportType
   *   Export type.
   * @param string $remoteId
   *   Remote object ID.
   */
  public function __construct(EntityInterface $entity, DestinationInterface $destination, $exportType, $remoteId) {
    $this->destination = $destination;
    $this->exportType = $exportType;
    $this->entity = $entity;
    $this->remoteObjectId = $remoteId;
  }

  /**
   * Get destination plugin.
   */
  public function getDestination() {
    return $this->destination;
  }

  /**
   * Export type getter.
   *
   * @return string
   *   Export type getter.
   */
  public function getExportType() {
    return $this->exportType;
  }

  /**
   * Entity getter.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Exported entity.
   */
  public function getEntity() {
    return $this->entity;
  }

  /**
   * Remote object ID getter.
   *
   * @return string
   *   Remote object ID.
   */
  public function getRemoteObjectId() {
    return $this->remoteObjectId;
  }

}
