<?php

namespace Drupal\dataflow\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\dataflow\Exception\ExportException;

/**
 * Entity sync error event.
 */
class EntitySyncError extends Event {

  const SYNC_ERROR = 'dataflow.sync_error';

  /**
   * Exception thrown.
   *
   * @var \Drupal\dataflow\Exception\ExportException
   */
  protected $exception;

  /**
   * EntitySyncError constructor.
   *
   * @param \Drupal\dataflow\Exception\ExportException $exception
   *   The export exception.
   */
  public function __construct(ExportException $exception) {
    $this->exception = $exception;
  }

  /**
   * Export exception getter.
   *
   * @return \Drupal\dataflow\Exception\ExportException
   *   The export exception thrown on entity sync.
   */
  public function getException() {
    return $this->exception;
  }

}
