<?php

namespace Drupal\dataflow\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Defines an interface for Dataflow sync plugins.
 */
interface SyncInterface extends PluginInspectionInterface {

  /**
   * Asserts that given entity matches sync plugin.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity object.
   *
   * @return self
   *   Self.
   */
  public function assertEntity(EntityInterface $entity);

  /**
   * Asserts that given entity should be synced.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity object.
   *
   * @return self
   *   Self.
   *
   * @throws \Drupal\dataflow\Exception\SyncExcludedException
   *   Syncing excluded.
   * @throws \Drupal\dataflow\Exception\RemovalRequestException
   *   Entity removal requested.
   */
  public function assertShouldSync(EntityInterface $entity);

  /**
   * Export given entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity object.
   *
   * @return \Drupal\dataflow\Status\ExportResult
   *   The export result object.
   *
   * @throws \Drupal\dataflow\Plugin\Exception\MissingPluginException
   *   Missing sync plugin of referenced entity.
   * @throws \Drupal\dataflow\Exception\SyncInProgressException
   *   Sync in progress.
   * @throws \Drupal\dataflow\Exception\Remote\RemoteException
   *   The remote call exception.
   * @throws \Drupal\dataflow\Exception\ExportException
   *   Export exceptions.
   */
  public function export(EntityInterface $entity);

  /**
   * Delete given exported entity from remote API.
   *
   * This method should be ONLY called if the entity exists in Drupal but should
   * be removed from remote API. It's NOT for removing objects from remote API
   * due to Drupal entity deletion.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity object.
   *
   * @throws \Drupal\dataflow\Exception\ExportException
   *   Export exceptions.
   */
  public function deleteFromRemote(EntityInterface $entity);

  /**
   * Get the destination plugin.
   *
   * @return \Drupal\dataflow\Plugin\DestinationInterface
   *   Destination plugin.
   */
  public function getDestination();

}
