<?php

namespace Drupal\dataflow\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\dataflow\Plugin\Exception\MissingPluginException;

/**
 * Sync plugin manager.
 */
class SyncPluginManager extends DefaultPluginManager implements SyncPluginManagerInterface {

  /**
   * Constructs a new EntitySyncManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheBackend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cacheBackend, ModuleHandlerInterface $moduleHandler) {
    parent::__construct('Plugin/dataflow/Sync', $namespaces, $moduleHandler, 'Drupal\dataflow\Plugin\SyncInterface', 'Drupal\dataflow\Annotation\DataflowSync');

    $this->alterInfo('dataflow_sync_info');
    $this->setCacheBackend($cacheBackend, 'dataflow_sync_plugins');
  }

  /**
   * {@inheritdoc}
   */
  public function getInstanceByType($entityType, $destinationId, $exportType = 'default') {
    foreach ($this->getDefinitions() as $definition) {
      // @todo Cache instances somehow?
      if ($definition['entityType'] == $entityType
        && $definition['destination'] == $destinationId
        && $definition['exportType'] == $exportType) {
        return $this->createInstance($definition['id']);
      }
    }

    throw new MissingPluginException();
  }

}
