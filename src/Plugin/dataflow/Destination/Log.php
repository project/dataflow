<?php

namespace Drupal\dataflow\Plugin\dataflow\Destination;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\dataflow\Plugin\DestinationPluginBase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Simple logging destination plugin.
 *
 * @DataflowDestination(
 *   id = "log",
 *   label = @Translation("Log"),
 *   description = @Translation("Logs all data export operations to Drupal log")
 * )
 */
class Log extends DestinationPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * The UUID service.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected UuidInterface $uuid;

  /**
   * Creates new Log object.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param array $plugin_definition
   *   The plugin definition.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid
   *   The UUID service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    array $plugin_definition,
    LoggerInterface $logger,
    UuidInterface $uuid
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->logger = $logger;
    $this->uuid = $uuid;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory')->get('dataflow'),
      $container->get('uuid')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function createObject(EntityInterface $entity, array $fields) {
    $id = $this->uuid->generate();

    $this->logger->info('Create object @id (Drupal entity @entity_id of type @entity_type): @fields', [
      '@id' => $id,
      '@entity_id' => $entity->id(),
      '@entity_type' => $entity->getEntityTypeId(),
      '@fields' => Json::encode($fields),
    ]);

    return $id;
  }

  /**
   * {@inheritdoc}
   */
  public function updateObject(EntityInterface $entity, $id, array $fields) {
    $this->logger->info('Update object @id (Drupal entity @entity_id of type @entity_type): @fields', [
      '@id' => $id,
      '@entity_id' => $entity->id(),
      '@entity_type' => $entity->getEntityTypeId(),
      '@fields' => Json::encode($fields),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteObject(EntityInterface $entity, $id) {
    $this->logger->info('Delete object @id (Drupal entity @entity_id of type @entity_type)', [
      '@id' => $id,
      '@entity_id' => $entity->id(),
      '@entity_type' => $entity->getEntityTypeId(),
    ]);
  }

}
