<?php

namespace Drupal\dataflow\Plugin;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for dataflow_destination plugins.
 */
abstract class DestinationPluginBase extends PluginBase implements DestinationInterface {}
