<?php

namespace Drupal\dataflow\Plugin\migrate\process;

use Drupal\dataflow\MappingManagerTrait;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Transforms Dataflow remote object ID to Drupal entity ID.
 *
 * @code
 * process:
 *   uid:
 *     plugin: dataflow_lookup
 *     destination: users_export
 *     entity_type: user
 *     export_type: default
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "dataflow_lookup"
 * )
 */
class DataflowLookup extends ProcessPluginBase {

  use MappingManagerTrait;

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (empty($this->configuration['destination'])) {
      throw new MigrateException('Missing destination configuration.');
    }
    if (empty($this->configuration['entity_type'])) {
      throw new MigrateException('Missing entity_type configuration.');
    }
    if (empty($this->configuration['export_type'])) {
      throw new MigrateException('Missing export_type configuration.');
    }

    $mapped_entities = $this->getMappingManager()->findMappedEntities($this->configuration['destination'], [$value]);
    $entityType = $this->configuration['entity_type'];
    $exportType = $this->configuration['export_type'];

    if (!empty($mapped_entities[$value][$entityType][$exportType])) {
      return $mapped_entities[$value][$entityType][$exportType];
    }

    return NULL;
  }

}
