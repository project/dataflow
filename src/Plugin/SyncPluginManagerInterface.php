<?php

namespace Drupal\dataflow\Plugin;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Dataflow sync plugin manager interface.
 */
interface SyncPluginManagerInterface extends PluginManagerInterface {

  /**
   * Get dataflow sync plugin for given entity type and destination plugin ID.
   *
   * @param string $entityType
   *   Entity type.
   * @param string $destinationId
   *   Destination plugin ID.
   * @param string $exportType
   *   Export type.
   *
   * @return \Drupal\dataflow\Plugin\SyncInterface|false
   *   Entity sync plugin.
   *
   * @throws \Drupal\dataflow\Plugin\Exception\MissingPluginException
   *   Missing sync plugin for given entity type and destination plugin.
   */
  public function getInstanceByType($entityType, $destinationId, $exportType = 'default');

}
