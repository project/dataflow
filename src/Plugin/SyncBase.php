<?php

namespace Drupal\dataflow\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\dataflow\Event\ExportEvent;
use Drupal\dataflow\Event\RemoteEntityReferenceEvent;
use Drupal\dataflow\Exception\PluginLogicException;
use Drupal\dataflow\Exception\Remote\PartiallyCreatedException;
use Drupal\dataflow\Exception\Remote\UpdatedObjectNotFoundException;
use Drupal\dataflow\Exception\RemovalRequestException;
use Drupal\dataflow\Exception\SyncExcludedException;
use Drupal\dataflow\Exception\SyncInProgressException;
use Drupal\dataflow\MappingManagerInterface;
use Drupal\dataflow\Status\ExportResult;
use Drupal\dataflow\Status\StoredExportStatus;
use Drupal\dataflow\SyncManagerInterface;
use Drupal\dataflow\Util\SyncKey;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Base class for Dataflow entity sync plugins.
 */
abstract class SyncBase extends PluginBase implements SyncInterface, ContainerFactoryPluginInterface {

  /**
   * Dataflow sync manager.
   *
   * @var \Drupal\dataflow\SyncManagerInterface
   */
  protected $syncManager;

  /**
   * ID mapper service.
   *
   * @var \Drupal\dataflow\MappingManagerInterface
   */
  protected $map;

  /**
   * Event dispatcher.
   *
   * @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The destination plugin manager.
   *
   * @var \Drupal\dataflow\Plugin\DestinationPluginManager
   */
  protected DestinationPluginManager $destinationManager;

  /**
   * Cached destination plugin.
   *
   * @var \Drupal\dataflow\Plugin\DestinationInterface
   */
  protected $destination;

  /**
   * Constructs sync plugin.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\dataflow\Plugin\DestinationPluginManager $destinationManager
   *   The destination plugin manager.
   * @param \Drupal\dataflow\SyncManagerInterface $syncManager
   *   The sync manager.
   * @param \Drupal\dataflow\MappingManagerInterface $map
   *   ID map service.
   * @param \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   The event dispatcher.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    DestinationPluginManager $destinationManager,
    SyncManagerInterface $syncManager,
    MappingManagerInterface $map,
    EventDispatcherInterface $eventDispatcher
  ) {
    $this->destinationManager = $destinationManager;
    $this->map = $map;
    $this->syncManager = $syncManager;
    $this->eventDispatcher = $eventDispatcher;
    return parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.dataflow_destination'),
      $container->get('dataflow.sync'),
      $container->get('dataflow.mapping'),
      $container->get('event_dispatcher'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function assertEntity(EntityInterface $entity) {
    $def = $this->getPluginDefinition();
    if ($entity->getEntityTypeId() != $def['entityType']) {
      throw new \LogicException('Incorrect entity type.');
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function export(EntityInterface $entity) {
    $exportStatus = $this->getExportStatus($entity);
    if (!$exportStatus || empty($exportStatus->getRemoteId())) {
      // Create new remote object.
      $exportResult = $this->recursiveCreate($entity);
      $this->assertRemoteId($exportResult->getRemoteId());
      // @todo Rework ExportEvent, use ExportResult object instead of
      //   $remoteId.
      $this->eventDispatcher->dispatch(new ExportEvent($entity, $this->getDestination(), $this->getExportType(), $exportResult->getRemoteId()), ExportEvent::CREATE);

      return $exportResult;
    }

    $destination = $this->getDestination();
    try {
      // Update existing remote object.
      $fields = $this->getDestinationFields($entity);
      $fieldsHash = $this->getFieldsHash($fields);

      // Only re-export if fields tracking is not enabled,
      // OR previously exported fields hash is not stored,
      // OR fields have changed.
      if (
        !$this->fieldsTrackingEnabled()
        || empty($exportStatus->getFieldsHash())
        || $exportStatus->getFieldsHash() !== $fieldsHash
      ) {
        // Fields have changed, update remote object.
        $destination->updateObject($entity, $exportStatus->getRemoteId(), $fields);
      }

      // @todo Rework ExportEvent, use ExportResult object instead of
      //   $remoteId.
      $this->eventDispatcher->dispatch(new ExportEvent($entity, $this->getDestination(), $this->getExportType(), $exportStatus->getRemoteId()), ExportEvent::UPDATE);

      return new ExportResult($exportStatus->getRemoteId(), $fieldsHash);
    }
    catch (UpdatedObjectNotFoundException $e) {
      if (!$this->recreateDeleted($entity)) {
        // Only recreate entity if told so.
        throw $e;
      }
      // Re-create object.
      $fields = $this->getDestinationFields($entity);
      $fieldsHash = $this->getFieldsHash($fields);
      $remoteId = $destination->createObject($entity, $fields);
      $this->assertRemoteId($remoteId);
      return new ExportResult($remoteId, $fieldsHash);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteFromRemote(EntityInterface $entity) {
    $destination = $this->getDestination();

    if (!($exportStatus = $this->getExportStatus($entity))) {
      $arguments = [
        '%plugin' => $this->getPluginId(),
      ];
      $message = (string) (new FormattableMarkup('The %plugin delete() method was called but no remote object ID found. This is probably a bug.', $arguments));
      throw new PluginLogicException($this->getEntityType(), $this->getDestination(), $this->getExportType(), $entity->id(), $message);
    }

    $destination->deleteObject($entity, $exportStatus->getRemoteId());
    // @todo Rework ExportEvent, use ExportResult object instead of
    //   $remoteId.
    $this->eventDispatcher->dispatch(new ExportEvent($entity, $this->getDestination(), $this->getExportType(), $exportStatus->getRemoteId()), ExportEvent::DELETE_REQUEST);
  }

  /**
   * {@inheritdoc}
   */
  public function assertShouldSync(EntityInterface $entity) {
    $should_sync = $this->shouldSync($entity);
    $should_delete = $this->shouldDelete($entity);

    if ($should_sync && $should_delete) {
      // Detect buggy plugins. If we don't do that we'd end up with some
      // entities exporting and removing back and forward.
      $arguments = [
        '%plugin' => $this->getPluginId(),
      ];
      $message = (string) (new FormattableMarkup('The %plugin requests both sync and removal at same time. This is a sync plugin bug.', $arguments));
      throw new PluginLogicException($this->getEntityType(), $this->getDestination(), $this->getExportType(), $entity->id(), $message);
    }

    // Removal requested.
    if ($should_delete) {
      if ($this->entityExported($entity)) {
        throw new RemovalRequestException($this->getEntityType(), $this->getDestination(), $this->getExportType(), $entity->id());
      }
    }

    // Item shouldn't be synced.
    if (!$should_sync) {
      throw new SyncExcludedException($this->getEntityType(), $this->getDestination(), $this->getExportType(), $entity->id());
    }

    // Proceed with sync.
    return $this;
  }

  /**
   * Creates a remote object, handling recursive dependencies.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to create.
   *
   * @return \Drupal\dataflow\Status\ExportResult
   *   Export result object.
   *
   * @throws \Drupal\dataflow\Exception\SyncInProgressException
   *   The sync have hit a circular dependency.
   * @throws \Drupal\dataflow\Exception\Remote\PartiallyCreatedException
   *   The remote object was partially created but some of its dependencies
   *   were not.
   * @throws \Drupal\dataflow\Exception\Remote\CreateException
   *   Thrown when the remote entity could not be created.
   */
  protected function recursiveCreate(EntityInterface $entity) {
    $skippedDependencies = [];

    try {
      // Try creating the remote object as is. In the best case, this will
      // just work as is.
      $fields = $this->getDestinationFields($entity);
      $remoteId = $this->getDestination()->createObject($entity, $fields);
    }
    catch (SyncInProgressException $e) {
      // One of the dependant entities is already being synced, meaning that
      // we've hit a dependency loop. Re-try, but omit the entity that caused
      // the exception, then try filling in missing relationships.
      $syncKey = $e->getSyncKey();
      $skippedDependencies[$syncKey] = $syncKey;
      $result = FALSE;

      while ($result === FALSE
        || $result instanceof SyncInProgressException) {
        // Try creating the entity again, but without the skipped dependencies.
        try {
          $fields = $this->getDestinationFields($entity, $skippedDependencies);
          $result = $this->getDestination()->createObject($entity, $fields);
        }
        catch (SyncInProgressException $e) {
          // Another failure, try to handle that.
          $result = $e;
          $syncKey = $result->getSyncKey();

          if (isset($skippedDependencies[$syncKey])) {
            // We've already tried to create this entity; this means we are in
            // the endless recursion. There's nothing we can do here anymore.
            // The parent sync process should catch this, otherwise, the sync
            // plugin should extend export() and/or recursiveCreate() to
            // implement its own logic handling this case.
            throw $e;
          }

          // Add the skipped dependency to the list of dependencies to try
          // creating again.
          $skippedDependencies[$syncKey] = $syncKey;
        }
      }

      // At this point, we've successfully created the entity without its
      // dependants. Let SyncManager know about it, so it could handle exporting
      // the dependants and updating the entity.
      throw new PartiallyCreatedException($this->getDestination(), $result, $skippedDependencies);
    }

    return new ExportResult($remoteId, $this->getFieldsHash($fields));
  }

  /**
   * Gets remote object ID of referenced entity.
   *
   * This method may lazily export the entity.
   *
   * @param string $entityType
   *   Entity type.
   * @param string $destinationId
   *   Destination plugin ID.
   * @param string $exportType
   *   Export type.
   * @param int $entityId
   *   Referencing entity ID.
   * @param array $skippedDependencies
   *   The list of dependencies that were skipped.
   * @param bool $required
   *   Whether this dependency is required. Passing FALSE allows partial export
   *   to resolve circular dependencies.
   *
   * @return string|false
   *   The remote object ID of referenced entity or FALSE if it shouldn't be
   *   exported at the moment.
   *
   * @throws \Drupal\dataflow\Plugin\Exception\MissingPluginException
   *   Missing sync plugin.
   * @throws \Drupal\dataflow\Exception\ExportException
   *   Referencing entity sync exception.
   * @throws \Drupal\dataflow\Exception\SyncInProgressException
   *   The entity is being synced and wasn't created yet.
   *
   *   This exception is caught at
   *   \Drupal\dataflow\SyncManager::sync(), you should NOT catch
   *   it.
   */
  protected function getReferencedEntityRemoteId($entityType, $destinationId, $exportType, $entityId, array $skippedDependencies = [], $required = FALSE) {
    $event = new RemoteEntityReferenceEvent(
      $entityType,
      $destinationId,
      $exportType,
      $entityId,
      $skippedDependencies,
      $required
    );
    $this->eventDispatcher->dispatch($event, RemoteEntityReferenceEvent::REMOTE_ENTITY_REFERENCE);

    // Add skipped dependencies.
    if (!empty($event->getSkippedDependencies())) {
      $skippedDependencies += $event->getSkippedDependencies();
    }
    if ($event->getRemoteEntityId()) {
      return $event->getRemoteEntityId();
    }

    $syncKey = SyncKey::getSyncKey($entityType, $destinationId, $exportType, $entityId);

    if (isset($skippedDependencies[$syncKey]) && !$required) {
      // This entity should be skipped for now; it would be exported later.
      return FALSE;
    }

    $syncStatus = $this->map->getSyncStatus($entityType, $destinationId, $exportType, $entityId);

    if (isset($syncStatus[$entityId]['status'])
      && $syncStatus[$entityId]['status'] == MappingManagerInterface::STATUS_IN_PROGRESS
      && empty($syncStatus[$entityId]['remote_id'])) {
      // Interrupt the infinite recursion.
      // This is the case when two entities are referencing each other.
      throw new SyncInProgressException($entityType, $destinationId, $exportType, $entityId);
    }

    return $this
      ->syncManager
      ->export($entityType, $destinationId, $exportType, $entityId)[$entityId];
  }

  /**
   * Get remote object ID of the entity exported by this plugin.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity object.
   *
   * @return \Drupal\dataflow\Status\StoredExportStatus|false
   *   Export status or FALSE.
   */
  protected function getExportStatus(EntityInterface $entity) {
    $status = $this
      ->map
      ->getSyncStatus($entity->getEntityTypeId(), $this->getDestination()->getPluginId(), $this->getExportType(), $entity->id());

    return isset($status[$entity->id()])
      ? StoredExportStatus::fromSyncStatus($status[$entity->id()])
      : FALSE;
  }

  /**
   * Get entity type from plugin definition.
   *
   * @return string
   *   Sync plugin entity type.
   */
  protected function getEntityType() {
    $plugin_definition = $this->getPluginDefinition();
    return $plugin_definition['entityType'];
  }

  /**
   * {@inheritdoc}
   */
  public function getDestination() {
    if (!isset($this->destination)) {
      $plugin_definition = $this->getPluginDefinition();

      $this->destination = $this
        ->destinationManager
        ->createInstance($plugin_definition['destination'], $plugin_definition['destinationSettings']);
    }

    return $this->destination;
  }

  /**
   * Get export type from plugin definition.
   *
   * @return string
   *   Sync plugin export type.
   */
  protected function getExportType() {
    $plugin_definition = $this->getPluginDefinition();
    return $plugin_definition['exportType'];
  }

  /**
   * Check if the entity is already exported.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity object.
   *
   * @return bool
   *   Whether the entity is exported.
   */
  protected function entityExported(EntityInterface $entity) {
    return (bool) $this->getExportStatus($entity);
  }

  /**
   * Check whether an entity deleted at remote should be re-created on export.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity being exported.
   *
   * @return bool
   *   Whether this entity should be re-created.
   */
  protected function recreateDeleted(EntityInterface $entity) {
    return FALSE;
  }

  /**
   * Get destination object fields.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity object.
   * @param array $skippedDependencies
   *   The list of dependencies that were skipped.
   *
   * @return array
   *   Array of fields for exporting.
   *
   * @throws \Drupal\dataflow\Exception\SyncInProgressException
   *   The referenced entity is currently being exported.
   *
   * @see \Drupal\dataflow\Plugin\SyncBase::getReferencedEntityRemoteId()
   */
  abstract protected function getDestinationFields(EntityInterface $entity, array $skippedDependencies = []);

  /**
   * Checks whether the entity can be synced or not.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity object.
   *
   * @return bool
   *   Whether the entity can be synced or not.
   */
  abstract protected function shouldSync(EntityInterface $entity);

  /**
   * Checks whether the entity should be removed from remote API.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity object.
   *
   * @return bool
   *   Whether the remote object should be removed.
   */
  abstract protected function shouldDelete(EntityInterface $entity);

  /**
   * Assert the remote ID is valid.
   *
   * @param string $remoteId
   *   The remote ID.
   */
  protected function assertRemoteId($remoteId) {
    if ($remoteId === NULL || $remoteId === FALSE || $remoteId === '') {
      throw new \InvalidArgumentException('Remote ID is not valid.');
    }
  }

  /**
   * Checks if field tracking is enabled for this plugin.
   */
  protected function fieldsTrackingEnabled() {
    $plugin_definition = $this->getPluginDefinition();
    return !empty($plugin_definition['trackFields']);
  }

  /**
   * Retrieves exported entity fields hash.
   *
   * @param array|null $fields
   *   Exported entity fields.
   *
   * @return string
   *   Exported entity fields hash. May return empty string if fields tracking
   *   is disabled.
   */
  protected function getFieldsHash($fields) {
    return $this->fieldsTrackingEnabled() ? ExportResult::fieldsHash($fields) : '';
  }

}
