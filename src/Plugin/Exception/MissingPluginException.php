<?php

namespace Drupal\dataflow\Plugin\Exception;

use Drupal\Component\Plugin\Exception\PluginException;

/**
 * Missing sync plugin exception.
 */
class MissingPluginException extends PluginException {}
