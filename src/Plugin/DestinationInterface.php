<?php

namespace Drupal\dataflow\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Interface for Dataflow destination plugin.
 */
interface DestinationInterface extends PluginInspectionInterface {

  /**
   * Creates remote entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The original exported Drupal entity.
   * @param array $fields
   *   Array of fields to be created.
   *
   * @return string
   *   The remote entity ID.
   *
   * @throws \Drupal\dataflow\Exception\Remote\CreateException
   *   Thrown when the remote entity could not be created.
   */
  public function createObject(EntityInterface $entity, array $fields);

  /**
   * Updates remote entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The original exported Drupal entity.
   * @param int $id
   *   The remote entity ID.
   * @param array $fields
   *   Array of fields to be updated.
   *
   * @throws \Drupal\dataflow\Exception\Remote\UpdateException
   *   The update failure exception.
   */
  public function updateObject(EntityInterface $entity, $id, array $fields);

  /**
   * Deletes remote entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The original exported Drupal entity.
   * @param int $id
   *   The remote entity ID.
   *
   * @throws \Drupal\dataflow\Exception\Remote\DeleteException
   *   The delete failure exception.
   */
  public function deleteObject(EntityInterface $entity, $id);

}
