<?php

namespace Drupal\dataflow\Util;

/**
 * The sync key utility class.
 */
final class SyncKey {

  /**
   * Get sync process key.
   *
   * @param string $entityType
   *   Entity type.
   * @param string $destinationId
   *   Destination plugin ID.
   * @param string $exportType
   *   Export type.
   * @param int $entityId
   *   Entity ID.
   *
   * @return string
   *   Sync process key.
   */
  public static function getSyncKey($entityType, $destinationId, $exportType, $entityId) {
    return implode(':', [$entityType, $destinationId, $exportType, $entityId]);
  }

}
