<?php

namespace Drupal\dataflow\Status;

/**
 * The export operation result class.
 */
final class ExportResult extends StatusBase {

  /**
   * Creates a new ExportResult object from remote ID and fields.
   *
   * @param string $remoteId
   *   The remote entity ID.
   * @param array|null $fields
   *   The exported entity fields.
   */
  public static function fromRemoteIdAndFields($remoteId, $fields) {
    return new self($remoteId, self::fieldsHash($fields));
  }

  /**
   * Calculates the hash of the exported entity fields.
   *
   * @param array|null $fields
   *   An array of exported entity fields.
   *
   * @return string|null
   *   The hash of the exported entity fields.
   */
  public static function fieldsHash($fields) {
    return $fields === NULL
      ? NULL
      : hash('sha256', serialize($fields));
  }

}
