<?php

namespace Drupal\dataflow\Status;

/**
 * Stored entity export status.
 */
final class StoredExportStatus extends StatusBase {

  /**
   * Creates new StoredExportStatus object from status row.
   *
   * @param array $syncStatus
   *   The sync status row from the database.
   *
   * @return \Drupal\dataflow\Status\StoredExportStatus
   *   The StoredExportStatus object.
   *
   * @see \Drupal\dataflow\MappingManagerInterface::getSyncStatus()
   */
  public static function fromSyncStatus(array $syncStatus) {
    return new self($syncStatus['remote_id'], $syncStatus['fields_hash']);
  }

}
