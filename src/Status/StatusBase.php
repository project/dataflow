<?php

namespace Drupal\dataflow\Status;

/**
 * Base abstract class for entity export status.
 */
abstract class StatusBase {

  /**
   * The remote entity ID.
   *
   * @var string
   */
  protected $remoteId;

  /**
   * Hash of the exported entity fields.
   *
   * @var string|null
   */
  protected $fieldsHash;

  /**
   * Constructs a new ExportResult object.
   *
   * @param string $remoteId
   *   The remote entity ID.
   * @param string|null $fieldsHash
   *   Hash of the exported entity fields.
   */
  public function __construct($remoteId, $fieldsHash = NULL) {
    $this->remoteId = $remoteId;
    $this->fieldsHash = $fieldsHash;
  }

  /**
   * Gets the remote entity ID.
   *
   * @return string
   *   The remote entity ID.
   */
  public function getRemoteId() {
    return $this->remoteId;
  }

  /**
   * Gets the hash of the exported entity fields.
   *
   * @return string|null
   *   The hash of the exported entity fields.
   */
  public function getFieldsHash() {
    return $this->fieldsHash;
  }

}
