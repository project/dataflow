<?php

namespace Drupal\Tests\dataflow\Unit;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Database\Connection;
use Drupal\dataflow\MappingManager;
use Drupal\dataflow\MappingManagerInterface;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * @coversDefaultClass \Drupal\dataflow\MappingManager
 * @group dataflow
 */
class MappingManagerTest extends UnitTestCase {

  /**
   * @covers ::__construct
   */
  public function testConstructor(): void {
    $mappingManager = $this->mockMappingManager();

    $this->assertInstanceOf(MappingManagerInterface::class, $mappingManager);
  }

  /**
   * @covers ::assertStatusValue
   * @dataProvider assertStatusValueDataProvider
   */
  public function testAssertStatusValue($status, $expectException): void {
    $mappingManager = $this->mockMappingManager();

    if ($expectException) {
      $this->expectException(\InvalidArgumentException::class);
    }

    $class = new \ReflectionClass($mappingManager);
    $method = $class->getMethod('assertStatusValue');
    $method->setAccessible(TRUE);
    $method->invokeArgs($mappingManager, [$status]);

    if (!$expectException) {
      $this->assertTrue(TRUE);
      ;
    }
  }

  /**
   * Returns the mocked mapping manager.
   */
  protected function mockMappingManager(): MappingManager {
    return new MappingManager(
      $this->getMockBuilder(Connection::class)
        ->disableOriginalConstructor()
        ->getMock(),
      $this->getMockBuilder(TimeInterface::class)->getMock(),
      $this->getMockBuilder(EventDispatcherInterface::class)->getMock()
    );
  }

  /**
   * Data provider for testAssertStatusValue().
   */
  public function assertStatusValueDataProvider() {
    $valid_statuses = [
      MappingManagerInterface::STATUS_NOT_SYNCED,
      MappingManagerInterface::STATUS_IN_PROGRESS,
      MappingManagerInterface::STATUS_SYNCED,
      MappingManagerInterface::STATUS_FAILED,
      MappingManagerInterface::STATUS_SYNC_EXCLUDED,
      MappingManagerInterface::STATUS_DELETION_IN_PROGRESS,
      MappingManagerInterface::STATUS_DELETED,
      MappingManagerInterface::STATUS_ENTITY_LOAD_ERROR,
    ];
    foreach ($valid_statuses as $status) {
      yield [$status, FALSE];
      yield [(string) $status, FALSE];
    }
    yield ['invalid_status', TRUE];
    yield [99, TRUE];
  }

  /**
   * @covers ::setSyncStatus
   */
  public function testSeSyncStatusInvalidStatusValue() {
    $mappingManager = $this->mockMappingManager();
    $this->expectException(\InvalidArgumentException::class);
    $mappingManager->setSyncStatus('node', 1, 'export', [1 => 1], 'invalid_status');
  }

}
