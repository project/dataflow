<?php

namespace Drupal\Tests\dataflow\Kernel\Destination;

use Drupal\Component\Serialization\Json;
use Drupal\dataflow\Plugin\dataflow\Destination\Log;
use Drupal\dataflow\Plugin\DestinationInterface;

/**
 * @coversDefaultClass \Drupal\dataflow\Plugin\dataflow\Destination\Log
 */
class LogDestinationTest extends DestinationTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'dataflow',
    'user',
    'dblog',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installSchema('dblog', ['watchdog']);
  }

  /**
   * {@inheritdoc}
   */
  protected function getDestinationPlugin(): DestinationInterface {
    return Log::create(
      $this->container,
      [],
      'log',
      [],
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getExportedItemsIds(): array {
    return array_diff($this->getCreatedItemsIds(), $this->getDeletedItemsIds());
  }

  /**
   * {@inheritdoc}
   */
  protected function getExportedItemFields(string $remoteId): ?array {
    $fields = NULL;

    foreach ($this->getLogEntries() as $entry) {
      if (preg_match('/Create|Update object @id \(Drupal entity @entity_id/', $entry->message)) {
        $variables = unserialize($entry->variables);
        $fields = Json::decode($variables['@fields']);
      }
    }

    return $fields;
  }

  /**
   * Returns IDs of created items.
   */
  protected function getCreatedItemsIds(): array {
    return array_filter(array_map(function ($row) {
      $variables = unserialize($row->variables);
      return preg_match('/Create object @id \(Drupal entity @entity_id/', $row->message)
        ? $variables['@entity_id']
        : NULL;
    }, $this->getLogEntries()));
  }

  /**
   * Returns IDs of deleted items.
   */
  protected function getDeletedItemsIds() {
    return array_filter(array_map(function ($row) {
      $variables = unserialize($row->variables);
      return preg_match('/Delete object @id \(Drupal entity @entity_id/', $row->message)
        ? $variables['@entity_id']
        : NULL;
    }, $this->getLogEntries()));
  }

  /**
   * Returns log entries.
   */
  protected function getLogEntries(): array {
    return $this
      ->container
      ->get('database')
      ->select('watchdog')
      ->fields('watchdog', ['message', 'variables'])
      ->condition('type', 'dataflow')
      ->orderBy('wid')
      ->execute()
      ->fetchAll();
  }

}
