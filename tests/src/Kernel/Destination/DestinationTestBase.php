<?php

namespace Drupal\Tests\dataflow\Kernel\Destination;

use Drupal\Core\Entity\EntityInterface;
use Drupal\dataflow\Plugin\DestinationInterface;
use Drupal\KernelTests\KernelTestBase;

/**
 * Base class for destination plugin tests.
 *
 * @coversDefaultClass \Drupal\dataflow\Plugin\DestinationPluginBase
 */
abstract class DestinationTestBase extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp():void {
    parent::setUp();

    $this->installSchema('dataflow', ['dataflow']);
    // User entity type is used for testing since it's the simplest type
    // available in core.
    $this->installEntitySchema('user');
  }

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'dataflow',
    'user',
  ];

  /**
   * Returns the destination plugin instance.
   *
   * This method must be implemented by the child class.
   */
  abstract protected function getDestinationPlugin(): DestinationInterface;

  /**
   * Returns IDs of items that have been exported.
   *
   * @return array
   *   An array of exported items destination IDs.
   */
  abstract protected function getExportedItemsIds(): array;

  /**
   * Returns remote object fields.
   *
   * @param string $remoteId
   *   The remote object ID.
   *
   * @return array|null
   *   An array of remote object fields.
   */
  abstract protected function getExportedItemFields(string $remoteId): ?array;

  /**
   * @covers ::__construct
   * @covers ::create
   */
  public function testInstantiate() {
    $this->assertInstanceOf(DestinationInterface::class, $this->getDestinationPlugin());
  }

  /**
   * @covers ::createObject
   */
  public function testCreateObject() {
    $fields = [
      'name' => 'test',
    ];
    $entity = $this->createTestEntity($fields);
    $entity->save();

    $destination = $this->getDestinationPlugin();

    $this->assertNotEmpty($destination->createObject($entity, $fields));
    $this->assertEquals([$entity->id()], $this->getExportedItemsIds());
  }

  /**
   * @covers ::deleteObject
   */
  public function testDeleteObject() {
    $fields = [
      'name' => 'test',
    ];
    $entity = $this->createTestEntity($fields);
    $entity->save();

    $destination = $this->getDestinationPlugin();

    $remoteId = $destination->createObject($entity, $fields);
    $destination->deleteObject($entity, $remoteId);

    $this->assertEmpty($this->getExportedItemsIds());
  }

  /**
   * @covers ::updateObject
   */
  public function testUpdateObject() {
    $fields = [
      'name' => 'created',
    ];
    $entity = $this->createTestEntity($fields);
    $entity->save();

    $destination = $this->getDestinationPlugin();

    $remoteId = $destination->createObject($entity, $fields);
    $destination->updateObject($entity, $remoteId, ['name' => 'updated']);

    $this->assertEquals(['name' => 'updated'], $this->getExportedItemFields($remoteId));
  }

  /**
   * Creates test entity.
   *
   * @param array $values
   *   Field values.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The test entity.
   */
  protected function createTestEntity(array $values): EntityInterface {
    return $this->container->get('entity_type.manager')
      ->getStorage('user')
      ->create($values);
  }

}
