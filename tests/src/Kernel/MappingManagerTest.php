<?php

namespace Drupal\Tests\dataflow\Kernel;

use Drupal\Component\Datetime\Time;
use Drupal\dataflow\MappingManagerInterface;
use Drupal\KernelTests\KernelTestBase;

/**
 * @coversDefaultClass \Drupal\dataflow\MappingManager
 * @group dataflow
 */
class MappingManagerTest extends KernelTestBase {

  /**
   * The constant value used to substitute current request time in tests.
   */
  const REQUEST_TIME = 'REQUEST_TIME';

  /**
   * {@inheritdoc}
   */
  protected function setUp():void {
    parent::setUp();

    $this->installSchema('dataflow', ['dataflow']);
  }

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'dataflow',
  ];

  /**
   * @covers ::countSyncQueue
   */
  public function testCountEmptySyncQueue(): void {
    // Make sure the queue is empty before we start.
    $this->assertAllQueuesAreEmpty();
  }

  /**
   * @covers ::getSyncStatus
   */
  public function testGetSyncStatusEmptyResult() {
    // The function return array with FALSE if entity was not exported.
    $notExportedSyncStatus = $this->getMappingManager()
      ->getSyncStatus('node', 'jsonapi', 'default', 14);
    $this->assertIsArray($notExportedSyncStatus);
    $this->assertCount(1, $notExportedSyncStatus);
    $this->assertFalse($notExportedSyncStatus[14]);

    // Or two FALSE entries if we've asked about two items.
    $notExportedSyncStatuses = $this->getMappingManager()
      ->getSyncStatus('node', 'jsonapi', 'default', [14, 'string_id']);
    $this->assertIsArray($notExportedSyncStatuses);
    $this->assertCount(2, $notExportedSyncStatuses);
    $this->assertFalse($notExportedSyncStatuses[14]);
    $this->assertFalse($notExportedSyncStatuses['string_id']);
  }

  /**
   * @covers ::getSyncStatus
   */
  public function testGetSyncStatusForNode() {
    $mappingManager = $this->getMappingManager();
    // Set the export status for one of the nodes and make sure it's being
    // returned.
    $mappingManager
      ->setSyncStatus(
        'node',
        'jsonapi',
        'default',
        [14 => 'remote_14'],
        MappingManagerInterface::STATUS_IN_PROGRESS,
        TRUE,
        [14 => 'hash_14']
      );
    $exportedStatuses = $mappingManager
      ->getSyncStatus('node', 'jsonapi', 'default', [14, 'string_id']);
    $this->assertIsArray($exportedStatuses);
    $this->assertCount(2, $exportedStatuses);
    $this->assertEquals([
      'status' => MappingManagerInterface::STATUS_IN_PROGRESS,
      'remote_id' => 'remote_14',
      // Sync time is 0 since the sync is in progress (i.e not finished yet).
      'sync_time' => 0,
      // Fail time is 0 since the sync didn't fail.
      'fail_time' => 0,
      'fields_hash' => 'hash_14',
      'entity_id' => 14,
    ], $exportedStatuses[14]);
    // The other item should still be FALSE.
    $this->assertFalse($exportedStatuses['string_id']);
  }

  /**
   * @covers ::getIdMap
   */
  public function testGetIdMap() {
    $mappingManager = $this->getMappingManager();

    // Default value for the entity which wasn't exported yet is FALSE.
    $this->assertEquals([22 => FALSE], $mappingManager
      ->getIdMap('node', 'jsonapi', 'default', 22));

    // Set the export status for one of the nodes and make sure it's being
    // returned.
    $mappingManager
      ->setSyncStatus(
        'node',
        'jsonapi',
        'default',
        [22 => 'remote_22'],
        MappingManagerInterface::STATUS_IN_PROGRESS,
      );
    $this->assertEquals([22 => 'remote_22'], $mappingManager
      ->getIdMap('node', 'jsonapi', 'default', 22));

    // The other item and export type should still be FALSE.
    $this->assertEquals([22 => FALSE, 23 => FALSE], $mappingManager
      ->getIdMap('node', 'jsonapi', 'another_type', [22, 23]));
  }

  /**
   * @covers ::setSyncStatus
   * @dataProvider setSyncStatusDataProvider
   */
  public function testSetSyncStatus(
    $entityType,
    $destinationId,
    $exportType,
    array $idMap,
    $status,
    $cronExport,
    array $fieldsHash,
    $expectedStatus
  ) {
    $mappingManager = $this->getMappingManager();
    $mappingManager->setSyncStatus(
      $entityType,
      $destinationId,
      $exportType,
      $idMap,
      $status,
      $cronExport,
      $fieldsHash,
    );

    foreach ($expectedStatus as &$entityStatus) {
      foreach (['sync_time', 'fail_time'] as $key) {
        if ($entityStatus[$key] === self::REQUEST_TIME) {
          $entityStatus[$key] = \Drupal::time()->getRequestTime();
        }
      }
    }

    $this->assertEquals($expectedStatus, $mappingManager->getSyncStatus(
      $entityType,
      $destinationId,
      $exportType,
      array_keys($idMap)
    ));
  }

  /**
   * Data provider for ::testSetSyncStatus().
   */
  public function setSyncStatusDataProvider(): array {
    return [
      [
        'node', 'jsonapi', 'default', [14 => 'remote_14'], MappingManagerInterface::STATUS_NOT_SYNCED, TRUE, [14 => 'hash_14'],
        [
          14 => [
            'status' => MappingManagerInterface::STATUS_NOT_SYNCED,
            'remote_id' => 'remote_14',
            // Sync time is 0 since the status is not synced.
            'sync_time' => 0,
            // Fail time is 0 since the sync didn't fail.
            'fail_time' => 0,
            'fields_hash' => 'hash_14',
            'entity_id' => 14,
          ],
        ],
      ],
      [
        'node', 'jsonapi', 'default', [14 => 'remote_14'], MappingManagerInterface::STATUS_IN_PROGRESS, TRUE, [14 => 'hash_14'],
        [
          14 => [
            'status' => MappingManagerInterface::STATUS_IN_PROGRESS,
            'remote_id' => 'remote_14',
            // Sync time is 0 since the sync is in progress.
            'sync_time' => 0,
            // Fail time is 0 since the sync didn't fail.
            'fail_time' => 0,
            'fields_hash' => 'hash_14',
            'entity_id' => 14,
          ],
        ],
      ],
      [
        'node', 'jsonapi', 'default', [14 => 'remote_14'], MappingManagerInterface::STATUS_SYNCED, TRUE, [14 => 'hash_14'],
        [
          14 => [
            'status' => MappingManagerInterface::STATUS_SYNCED,
            'remote_id' => 'remote_14',
            // Sync time is current request, the sync is finished.
            'sync_time' => self::REQUEST_TIME,
            // Fail time is 0 since the sync didn't fail.
            'fail_time' => 0,
            'fields_hash' => 'hash_14',
            'entity_id' => 14,
          ],
        ],
      ],
      [
        'node', 'jsonapi', 'default', [14 => 'remote_14'], MappingManagerInterface::STATUS_FAILED, TRUE, [14 => 'hash_14'],
        [
          14 => [
            'status' => MappingManagerInterface::STATUS_FAILED,
            'remote_id' => 'remote_14',
            // Sync have failed, so the sync time is 0.
            'sync_time' => 0,
            // Fail time is current request time.
            'fail_time' => self::REQUEST_TIME,
            'fields_hash' => 'hash_14',
            'entity_id' => 14,
          ],
        ],
      ],
      [
        'node', 'jsonapi', 'default', [14 => 'remote_14'], MappingManagerInterface::STATUS_SYNC_EXCLUDED, TRUE, [14 => 'hash_14'],
        [
          14 => [
            'status' => MappingManagerInterface::STATUS_SYNC_EXCLUDED,
            'remote_id' => 'remote_14',
            // The item is not synced, so sync time is 0.
            'sync_time' => 0,
            // Fail time is 0 since the sync didn't fail.
            'fail_time' => 0,
            'fields_hash' => 'hash_14',
            'entity_id' => 14,
          ],
        ],
      ],
      [
        'node', 'jsonapi', 'default', [14 => 'remote_14'], MappingManagerInterface::STATUS_DELETION_IN_PROGRESS, TRUE, [14 => 'hash_14'],
        [
          14 => [
            'status' => MappingManagerInterface::STATUS_DELETION_IN_PROGRESS,
            'remote_id' => 'remote_14',
            // Deletion is in progress.
            'sync_time' => 0,
            // Fail time is 0 since the sync didn't fail.
            'fail_time' => 0,
            'fields_hash' => 'hash_14',
            'entity_id' => 14,
          ],
        ],
      ],
      [
        'node', 'jsonapi', 'default', [14 => 'remote_14'], MappingManagerInterface::STATUS_DELETED, TRUE, [14 => 'hash_14'],
        [
          14 => [
            'status' => MappingManagerInterface::STATUS_DELETED,
            // We don't have remote ID anymore as the item was deleted.
            'remote_id' => NULL,
            // Item was deleted from remote.
            'sync_time' => 0,
            // Fail time is 0 since the sync didn't fail.
            'fail_time' => 0,
            'fields_hash' => 'hash_14',
            'entity_id' => 14,
          ],
        ],
      ],
    ];
  }

  /**
   * @covers ::getSyncQueue
   * @covers ::getSyncQueueQuery
   * @dataProvider getSyncQueueDataProvider
   */
  public function testGetSyncQueue(array $syncItems, array $expectedQueue, ?bool $cronExport, int $limit = 100) {
    $mappingManager = $this->getMappingManager();

    foreach ($syncItems as $syncItem) {
      $mappingManager->setSyncStatus(...$syncItem);
    }

    $this->assertEquals($expectedQueue, $mappingManager->getSyncQueue($limit, $cronExport));
  }

  /**
   * Data provider for ::testGetSyncQueue().
   */
  public function getSyncQueueDataProvider() {
    $syncItem = [
      'node',
      'jsonapi',
      'default',
      [14 => 'remote_14'],
      MappingManagerInterface::STATUS_NOT_SYNCED,
      TRUE,
    ];
    $singleSyncItemQueue = ['node' => ['jsonapi' => ['default' => [14 => 14]]]];
    $allowedStatuses = [
      MappingManagerInterface::STATUS_NOT_SYNCED,
      MappingManagerInterface::STATUS_IN_PROGRESS,
      MappingManagerInterface::STATUS_DELETION_IN_PROGRESS,
    ];
    $excludedStatuses = [
      MappingManagerInterface::STATUS_SYNCED,
      MappingManagerInterface::STATUS_SYNC_EXCLUDED,
      MappingManagerInterface::STATUS_DELETED,
      MappingManagerInterface::STATUS_ENTITY_LOAD_ERROR,
    ];

    // The queue is empty by default.
    yield  [[], [], NULL];
    yield  [[], [], TRUE];
    yield  [[], [], FALSE];

    // Add one item, check that it is in the queue (but only in Cron queue).
    yield [[$syncItem], $singleSyncItemQueue, NULL];
    yield [[$syncItem], $singleSyncItemQueue, TRUE];
    yield [[$syncItem], [], FALSE];

    // Now do the same for non-Cron queue.
    $syncItem[5] = FALSE;
    yield [[$syncItem], $singleSyncItemQueue, NULL];
    yield [[$syncItem], [], TRUE];
    yield [[$syncItem], $singleSyncItemQueue, FALSE];

    // Test the limit. Push two items, limit to 1, make sure only one is
    // returned.
    $anotherSyncItem = $syncItem;
    $anotherSyncItem[3] = [15 => 'remote_15'];
    yield [[$syncItem, $anotherSyncItem], $singleSyncItemQueue, NULL, 1];

    // Test allowed statuses.
    foreach ($allowedStatuses as $allowedStatus) {
      $syncItem[4] = $allowedStatus;
      yield [[$syncItem], $singleSyncItemQueue, NULL];
    }

    // Test excluded statuses.
    foreach ($excludedStatuses as $excludedStatus) {
      $syncItem[4] = $excludedStatus;
      yield [[$syncItem], [], NULL];
    }
  }

  /**
   * @covers ::getSyncQueue
   * @covers ::getSyncQueueQuery
   */
  public function testSyncQueueForFailedItem() {
    $this->getMappingManager()->setSyncStatus(
      'node',
      'jsonapi',
      'default',
      [14 => 'remote_14'],
      MappingManagerInterface::STATUS_FAILED,
    );

    // The failed item should not be in the queue.
    $this->assertAllQueuesAreEmpty();
  }

  /**
   * @covers ::getSyncQueue
   * @covers ::getSyncQueueQuery
   */
  public function testSyncQueueRetryForFailedItem() {
    $mockedTime = 1000;

    $timeMock = $this->getMockBuilder(Time::class)
      ->disableOriginalConstructor()
      ->onlyMethods(['getRequestTime'])
      ->getMock();
    $timeMock
      ->method('getRequestTime')
      ->willReturnCallback(function () use (&$mockedTime) {
        return $mockedTime;
      });
    $this->container->set('datetime.time', $timeMock);

    $this->getMappingManager()->setSyncStatus(
      'node',
      'jsonapi',
      'default',
      [14 => 'remote_14'],
      MappingManagerInterface::STATUS_FAILED,
    );

    $mockedTime = time();

    // The failed item should be in the queue if it failed long time ago.
    $this->assertEquals(
      ['node' => ['jsonapi' => ['default' => [14 => 14]]]],
      $this->getMappingManager()->getSyncQueue(1, NULL)
    );
  }

  /**
   * Asserts that mapping manager queue is empty.
   */
  protected function assertAllQueuesAreEmpty(): void {
    $mappingManager = $this->getMappingManager();
    $this->assertEquals(0, $mappingManager->countSyncQueue(NULL));
    $this->assertEquals(0, $mappingManager->countSyncQueue(TRUE));
    $this->assertEquals(0, $mappingManager->countSyncQueue(FALSE));
  }

  /**
   * @covers ::findMappedEntities
   */
  public function testFindMappedEntitiesTest() {
    $mappingManager = $this->getMappingManager();

    // First, make sure the returned array is a list of FALSE values keyed by
    // remote IDs if there are no mappings.
    $this->assertEquals(
      ['remote_1' => FALSE, 'another_remote_2' => FALSE],
      $mappingManager
        ->findMappedEntities('jsonapi', ['remote_1', 'another_remote_2'])
    );

    // Then, set sync status for few nodes (of different types and different
    // destinations and export types) and make sure the returned array contains
    // the expected values.
    $mappingManager->setSyncStatus(
      'node',
      'jsonapi',
      'default',
      [1 => 'remote_1', 2 => 'remote_2'],
      MappingManagerInterface::STATUS_SYNCED,
    );
    $mappingManager->setSyncStatus(
      'node',
      'jsonapi',
      'another_export_type',
      [10 => 'remote_1', 3 => ' '],
      MappingManagerInterface::STATUS_SYNCED,
    );
    $mappingManager->setSyncStatus(
      'another_entity_type',
      'jsonapi',
      'default',
      [1 => 'remote_1', 5 => 'remote_5'],
      MappingManagerInterface::STATUS_FAILED,
    );
    $mappingManager->setSyncStatus(
      'node',
      'another_destination',
      'default',
      [1 => 'remote_1', 4 => 'remote_4'],
      MappingManagerInterface::STATUS_IN_PROGRESS,
    );

    $this->assertEquals(
      [
        'remote_1' => [
          'node' => [
            'default' => 1,
            'another_export_type' => 10,
          ],
          'another_entity_type' => [
            'default' => 1,
          ],
        ],
        'remote_2' => [
          'node' => [
            'default' => 2,
          ],
        ],
        'remote_4' => FALSE,
      ],
      // The remote_3 key is intentionally not queried to make sure it's not in
      // the list. The remote_4 is queried, but it was synced to a different
      // destination, so it's FALSE.
      $mappingManager
        ->findMappedEntities('jsonapi', ['remote_1', 'remote_2', 'remote_4'])
    );

    // Now test the same logic with a different destination.
    $this->assertEquals(
      [
        'remote_4' => [
          'node' => [
            'default' => 4,
          ],
        ],
      ],
      $mappingManager
        ->findMappedEntities('another_destination', ['remote_4'])
    );
  }

  /**
   * Returns the Mapping Manager service.
   *
   * @return \Drupal\dataflow\MappingManagerInterface
   *   The Mapping Manager service.
   */
  protected function getMappingManager(): MappingManagerInterface {
    return $this->container->get('dataflow.mapping');
  }

}
